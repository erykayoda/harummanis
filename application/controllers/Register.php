<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Register extends CI_Controller {

 function __construct()
 {
   parent::__construct();
   $this->load->model('user','',TRUE);
 }

 function index()
 {
   //This method will have the credentials validation
   $this->load->helper('security');
   $this->load->library('form_validation');

   $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean|callback_check_uname');
   $this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');
   $this->form_validation->set_rules('addr', 'Addr', 'trim|required|xss_clean');
   $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email');
   $this->form_validation->set_rules('phone', 'Phone', 'trim|required|xss_clean|numeric');
   $this->form_validation->set_rules('password' , 'Password', 'trim|required|xss_clean|matches[passconf]|min_length[8]');
   $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required');

   if($this->form_validation->run() == FALSE)
   {
     //Field validation failed.  User redirected to login page
     $this->load->view('register');
   }
   else
   {
     $id = '';
     $gen = range(0, 9);
     for ($i = 0; $i < 4; $i++) {
       $id .= $gen[array_rand($gen)];
     }
     $rid = 'MEM'.$id;

    $info = array(
      'uname' => $this->input->post('username'),
      'userid' => $rid,
      'level' => 'USER',
      'passwd' => $this->input->post('password'),
      'email'  => $this->input->post('email'),
      'phone'  => $this->input->post('phone'),
      'addr'  => $this->input->post('addr'),
    );

    $this->user->addUsr($info);
    $this->load->view('login');
   }

 }

 function check_uname($uname)
 {
   //Field validation succeeded.  Validate against database
   $username = $this->input->post('username');

   //query the database
   $result = $this->user->chk($uname);

   if($result == TRUE)
   {
     return true;
   }
   else
   {
     $this->form_validation->set_message('check_uname', 'This username is not available');
     return FALSE;
   }
 }
}
?>
