<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class ManagePro extends CI_Controller {

 function __construct()
 {
   parent::__construct();
	 $this->load->model('Product','',TRUE);

 }

 function index()
 {
   if($this->session->userdata('logged_in'))
   {
     $session_data = $this->session->userdata('logged_in');
     $data  = array(
       'level' => $session_data['level'],
       'username' => $session_data['username'],
       'list' => $this->Product->comList()
     );
     //$data['username'] = $session_data['username'];
     $this->load->view('admin/managePro', $data);
   }
   else
   {
     //If no session, redirect to login page
     redirect('admin/login', 'refresh');
   }
 }


function loadData(){
  if($this->session->userdata('logged_in'))
  {

  function checkStatus(&$fck){
    if ($fck == '0'){
      return '<span class="label label-danger">OUT OF STOCK!</span>';
    }
    else if($fck == '1') {
      return '<span class="label label-success">IN STOCK</span>';
    }
    else if($fck == 'PENDING'){
      return '<span class="label label-warning">Awaiting for payment..</span>';
    }
  }
  function negaval($hai){
    if ($hai < 0){
      return '<span class="label label-danger">'.$hai.'</span>';
    }
    else {
      return $hai;
    }
  }
  $list = $this->Product->getData();
	//$data = array();
	 foreach ($list as $get) {
     $data[] = array(
       'id' => $get->proid,
       'name' => $get->name,
       'price' => 'RM ' . number_format($get->price,2),
       'sprice' => 'RM ' . number_format($get->sprice,2),
       'gpro' => 'RM ' . negaval(number_format($get->gprofit,2)),
       'issue' => $get->issue,
       'comName' => $get->companyName,
       'stock' => checkStatus($get->stat),
       'action' => '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="viewpro('."'".$get->proid."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
             <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="deletePro('."'".$get->proid."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>'
     );
   }
   echo json_encode($data);
 }
 else
  {
   //If no session, redirect to login page
   redirect('login', 'refresh');
  }
 }


function updatePro()
{

  if($this->session->userdata('logged_in'))
  {

   $data = array(
   'proid' => $this->input->post('id'),
   'vendorID' => $this->input->post('sup'),
   'name' => $this->input->post('proName'),
   'info' => $this->input->post('info'),
   'type' => $this->input->post('type'),
   'stat' => $this->input->post('stat'),
   'price' => $this->input->post('basePrice'),
   'sprice' => $this->input->post('sellPrice'),
   'gprice' => $this->input->post('sellPrice') - $this->input->post('basePrice')
   );

   $this->load->model('product'); // First load the model
   $data = $this->security->xss_clean($data);
   if($this->Product->updatePro($data)) // call the method from the controller
     {
         // update successful...
         echo json_encode(array("status" => 'SUCCESS'));
     }
     else
     {
         echo json_encode(array("status" => 'FAILED'));
     }
   }
   else {
    header("HTTP/1.1 404 Not Found");
    echo "404 not found";
   }
}


function add(){

  if($this->session->userdata('logged_in'))
  {

  $session_data = $this->session->userdata('logged_in');
  $data['username'] = $session_data['username'];

  $id = '';
  $gen = range(0, 9);
  for ($i = 0; $i < 5; $i++) {
    $id .= $gen[array_rand($gen)];
  }
  $rid = 'PRO'.$id;
  $data = array(
  'proid' => $rid,
  'vendorID' => $this->input->post('sup'),
  'name' => $this->input->post('proName'),
  'proInfo' => $this->input->post('info'),
  'type' => $this->input->post('type'),
  'stat' => $this->input->post('stat'),
  'price' => $this->input->post('basePrice'),
  'sprice' => $this->input->post('sellPrice'),
  'gprofit' => $this->input->post('sellPrice') - $this->input->post('basePrice'),
  'issue' => $data['username'],
  );

  $this->load->model('Product'); // First load the model
  $data = $this->security->xss_clean($data);
  if($this->Product->savePro($data)) // call the method from the controller
    {
        // update successful...
        echo json_encode(array("status" => 'SUCCESS'));
    }
    else
    {
        echo json_encode(array("status" => 'FAILED'));
    }
  }
    else {
     header("HTTP/1.1 404 Not Found");
     echo "404 not found";
    }

}

public function detail($id)
{
  if($this->session->userdata('logged_in'))
  {


  $data = $this->Product->getInfo($id);
  //var_dump($data);
  //$data->dob = ($data->dob == '0000-00-00') ? '' : $data->dob; // if 0000-00-00 set tu empty for datepicker compatibility
  echo json_encode($data);
}
  else {
   header("HTTP/1.1 404 Not Found");
   echo "404 not found";
  }

}

 function deletePro($id){
   if($this->session->userdata('logged_in')){
     if(isset($id)){
       $data = $this->Product->deleteInfo($id);
       echo json_encode($data);
     }
     else {
       header("HTTP/1.1 404 Not Found");
       echo "404 not found";
     }

 }
   else {
    header("HTTP/1.1 404 Not Found");
    echo "404 not found";
   }

 }




}

?>
