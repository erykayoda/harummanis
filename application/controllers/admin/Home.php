<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Home extends CI_Controller {

 function __construct()
 {
   parent::__construct();
	 $this->load->model('Anal','',TRUE);
 }

 function index()
 {
   if($this->session->userdata('logged_in'))
   {
     $session_data = $this->session->userdata('logged_in');
     $data  = array(
       'level' => $session_data['level'],
       'username' => $session_data['username'],
       'pending' => $this->Anal->pending(),
       'deliver' => $this->Anal->deliver(),
       'sales' => $this->Anal->sales(),
       'total' => $this->Anal->total()
     );
     $this->load->view('admin/home_views',$data);
   }
   else
   {
     //If no session, redirect to login page
     redirect('admin/login', 'refresh');
   }
 }


 function analytic(){
   $this->load->view("admin/static");
 }

 function manageOrder(){
   $this->load->view('admin/manageOrder');
 }

 function logout()
 {
   $this->session->unset_userdata('logged_in');
   session_destroy();
   redirect('admin/home', 'refresh');
 }

}

?>
