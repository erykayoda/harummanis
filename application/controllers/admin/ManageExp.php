<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class ManageExp extends CI_Controller {

 function __construct()
 {
   parent::__construct();
	 $this->load->model('Product','',TRUE);
   $this->load->model('Expense','',TRUE);

 }

 function index()
 {
   if($this->session->userdata('logged_in'))
   {
     $session_data = $this->session->userdata('logged_in');
     $data  = array(
       'level' => $session_data['level'],
       'username' => $session_data['username'],
       'list' => $this->Expense->typeList(),
       'company' => $this->Expense->comList()
     );
     //$data['username'] = $session_data['username'];
     $this->load->view('admin/manageExp', $data);
   }
   else
   {
     //If no session, redirect to login page
     redirect('admin/login', 'refresh');
   }
 }

 function type(){

   if($this->session->userdata('logged_in'))
   {
   $list = $this->Expense->type();
 	//$data = array();
 	 foreach ($list as $get) {
      $data[] = array(
        'id' => $get->typID,
        'name' => $get->typeName,
        'info' => $get->info,
        'issue' => $get->uname,
        'action' => '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="viewExp('."'".$get->typID."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
              <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="deleteTyp('."'".$get->typID."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>'
      );
    }
    echo json_encode($data);
  }
  else
   {
     header("HTTP/1.1 404 Not Found");
     echo "404 not found";
   }
 }

function loadData(){
  if($this->session->userdata('logged_in'))
  {

  function checkStatus(&$fck){
    if ($fck == '0'){
      return '<span class="label label-danger">UNPAID</span>';
    }
    else if($fck == '1') {
      return '<span class="label label-success">PAID</span>';
    }
  }

  $list = $this->Expense->getData();
	//$data = array();
	 foreach ($list as $get) {
     $data[] = array(
       'id' => $get->expid,
       'venid' => $get->vendorID,
       'name' => $get->name,
       'type' => $get->typeName,
       'issue' => $get->uname,
       'total' => 'RM ' . number_format($get->total,2),
       'bila' => $get->date_create,
       'remarks' => $get->remarks,
       'status' => checkStatus($get->stat),
       'action' => '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="viewExp('."'".$get->expid."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
             <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="deleteExp('."'".$get->expid."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>'
     );
   }
   echo json_encode($data);
 }
 else
  {
    header("HTTP/1.1 404 Not Found");
    echo "404 not found";
  }
 }

 function addType(){
if($this->session->userdata('logged_in'))
{
  $session_data = $this->session->userdata('logged_in');
  $userid = $session_data['userid'];
  $data = array(
    'typeName' => $this->input->post('name'),
    'info' => $this->input->post('info'),
    'userid' => $userid
  );
  $data = $this->security->xss_clean($data);
  $this->load->model('Expense');
  if($this->Expense->typeAdd($data)) // call the method from the controller
  {
        // update successful...
        echo json_encode(array("status" => 'SUCCESS'));
   }
   else
    {
      echo json_encode(array("status" => 'FAILED'));

    }
 }else {
  header("HTTP/1.1 404 Not Found");
  echo "404 not found";
}

 }

function updateExpense()
{
  if($this->session->userdata('logged_in'))
  {
    if ($this->input->post('expname') == NULL){
     echo json_encode(array(
       "status" => "ERR",
       "msg" => "Please enter expense name"
     ));
   }
   else if ($this->input->post('total') == NULL){
     echo json_encode(array(
       "status" => "ERR",
       "msg" => "Please enter total amount of expense"
     ));
   }
   else if(!filter_var($this->input->post('total'), FILTER_VALIDATE_FLOAT)){
     echo json_encode(array(
       "status" => "ERR",
       "msg" => "Invalid amount format"
     ));
   }
   else if ($this->input->post('info') == NULL){
     echo json_encode(array(
       "status" => "ERR",
       "msg" => "Plese enter info of expense"
     ));
   }
   else {
     $data = array(
       'expid' => $this->input->post('id'),
       'vendorID' => $this->input->post('ven'),
       'name' => $this->input->post('expname'),
       'total' => $this->input->post('total'),
       'remarks' => $this->input->post('info'),
       'stat' => $this->input->post('stat'),
       'typID' => $this->input->post('typ'),
       'total' => $this->input->post('total'),
     );

     $data = $this->security->xss_clean($data);
     if($this->Expense->updateExp($data)) // call the method from the controller
      {
           // update successful...
           echo json_encode(array("status" => 'SUCCESS'));
       }
       else
       {
           echo json_encode(array("status" => 'FAILED'));
       }
   }

  }else {
    header("HTTP/1.1 404 Not Found");
    echo "404 not found";
  }

}


function add(){


  if($this->session->userdata('logged_in'))
  {
    if ($this->input->post('expname') == NULL){
     echo json_encode(array(
       "status" => "ERR",
       "msg" => "Please enter expense name"
     ));
   }
   else if ($this->input->post('total') == NULL){
     echo json_encode(array(
       "status" => "ERR",
       "msg" => "Please enter total amount of expense"
     ));
   }
   else if(!filter_var($this->input->post('total'), FILTER_VALIDATE_FLOAT)){
     echo json_encode(array(
       "status" => "ERR",
       "msg" => "Invalid amount format"
     ));
   }
   else if ($this->input->post('info') == NULL){
     echo json_encode(array(
       "status" => "ERR",
       "msg" => "Plese enter info of expense"
     ));
   }
   else {
    $session_data = $this->session->userdata('logged_in');
    $userid = $session_data['userid'];

    $id = '';
    $gen = range(0, 9);
    for ($i = 0; $i < 5; $i++) {
      $id .= $gen[array_rand($gen)];
    }
    $rid = 'EXP'.$id;
    $data = array(
    'expid' => $rid,
    'vendorID' => $this->input->post('ven'),
    'name' => $this->input->post('expname'),
    'total' => $this->input->post('total'),
    'remarks' => $this->input->post('info'),
    'stat' => $this->input->post('stat'),
    'typID' => $this->input->post('typ'),
    'userid' => $userid,
    );
    $data = $this->security->xss_clean($data);
    if($this->Expense->saveExp($data)) // call the method from the controller
      {
          // update successful...
          echo json_encode(array("status" => 'SUCCESS'));
      }
      else
      {
          echo json_encode(array("status" => 'FAILED'));
      }
    }
  }else {
    header("HTTP/1.1 404 Not Found");
    echo "404 not found";
  }



}

public function detail($id)
{
  if($this->session->userdata('logged_in')){
  $data = $this->Expense->getInfo($id);
  echo json_encode($data);
}else {
  header("HTTP/1.1 404 Not Found");
  echo "404 not found";
}
}

 function delete($id){
   if($this->session->userdata('logged_in')){

   $data = $this->Expense->deleteInfo($id);
   echo json_encode($data);
 }else {
   header("HTTP/1.1 404 Not Found");
   echo "404 not found";
 }

 }

 function deleteType($id){
   if($this->session->userdata('logged_in')){
   $chk = $this->Expense->chk($id);
   if($chk > 0){
     echo json_encode(array(
       "status" => "ERR",
       "msg" => "There is data associated with this item, please delete/update the related data before proceed!"
     ));
   }else {
     $data = $this->Expense->deleteTyp($id);
     echo json_encode($data);
   }

 }else {
   header("HTTP/1.1 404 Not Found");
   echo "404 not found";
 }

 }

}

?>
