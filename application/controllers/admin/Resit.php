<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Resit extends CI_Controller {

 function __construct()
 {
   parent::__construct();
	 $this->load->model('Order','',TRUE);
   $this->load->library('pdf');

 }

 function index()
 {
   if($this->session->userdata('logged_in'))
   {
     $session_data = $this->session->userdata('logged_in');
     $data['username'] = $session_data['username'];
     $this->load->view('admin/invoice', $data);
   }
   else
   {
     //If no session, redirect to login page
     redirect('admin/login', 'refresh');
   }
 }

function itemOrder($id){

  $this->db->select('entry_id, comments.id AS comment_id, comment, title');
  $this->db->from('comments');
  $this->db->join('posts', 'comments.entry_id = posts.id');
  $query = $this->db->get();

  return $query->result;
}
 function com_control() {

 }
 function loadData(){

   function checkStatus(&$fck){
     if ($fck == 'NORES'){
        return '<span class="label label-danger">Transaction failed</span>';
     }
     else if($fck == 'DELIVERED') {
        return '<span class="label label-success">Order Successfull</span>';
     }
     else if($fck == 'PENDING'){
       return '<span class="label label-warning">Awaiting for payment..</span>';
     }
   }

	 $list = $this->Order->getData();
	 //$data = array();
	 foreach ($list as $get) {
            //$no++;
            $data[] = array(
              'id' => $get->orderid,
              'userid' => $get->userid,
              'name' => $get->name,
              'del' => $get->del_mthd,
              'addr' => $get->addr,
              'type' => $get->type,
              'qty' => $get->qty,
              'date' => $get->date,
              'remarks' => $get->orderid,
              'total' => '<b>RM '.number_format($get->total,2).'</b>',
              'status' => checkStatus($get->status),
              'action' => '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="view('."'".$get->orderid."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
                    <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('."'".$get->orderid."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>'
            );
        }


        //output to json format
        echo json_encode($data);

 }

 function genpdf($id){
   //$id = $this->input->post('id');
   $far = $this->Order->test($id);
   foreach ($far as $get) {
     $data = array(
       'id' => $get->orderid,
       'userid' => $get->userid,
       'name' => $get->name,
       'del' => $get->del_mthd,
       'addr' => $get->addr,
       'type' => $get->type,
       'qty' => $get->qty,
       'date' => $get->date,
       'email' => $get->email,
       'remarks' => $get->remarks,
       'total' => '<b>RM '.number_format($get->total,2).'</b>',
     );
   }
   $session_data = $this->session->userdata('logged_in');
   //$data['username'] = $session_data['username'];
   $this->load->library('pdf');
   $this->pdf->load_view('invoice' , $data);
  }



}

?>
