<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class ManageSpp extends CI_Controller {

 function __construct()
 {
   parent::__construct();
	 $this->load->model('Supplier','',TRUE);

 }

 function index()
 {
   if($this->session->userdata('logged_in'))
   {
     $session_data = $this->session->userdata('logged_in');
     $data['username'] = $session_data['username'];
     $this->load->view('admin/manageStk', $data);
   }
   else
   {
     //If no session, redirect to login page
     redirect('admin/login', 'refresh');
   }
 }

 function loadData(){
    if($this->session->userdata('logged_in')){
      $list = $this->Supplier->getData();
      //$data = array();
      foreach ($list as $get) {
        $data[] = array(
          'id' => $get->vendorID,
          'name' => $get->companyName,
          'email' => $get->email,
          'adddr' => $get->address,
          'tel' => $get->tel,
          'info' => $get->info,
          'ssm' => $get->ssmNo,
          'pic' => $get->pic,
          'date' => $get->created,
          'issue' => $get->issued,
          'action' => '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="displayCom('."'".$get->vendorID."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
                <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="deleteCom('."'".$get->vendorID."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>'
        );
    }
    echo json_encode($data);
  }else {
    header("HTTP/1.1 404 Not Found");
    echo "404 not found";
  }

}

function deleteCom($id){
  $data = $this->Supplier->deleteComInfo($id);
  echo json_encode($data);
}

 function add(){

   if ($this->session->userdata('logged_in')) {

      if ($this->input->post('name') == NULL){
       echo json_encode(array(
         "status" => "ERR",
         "msg" => "Please enter your name"
       ));
     }
     else if ($this->input->post('email') == NULL){
       echo json_encode(array(
         "status" => "ERR",
         "msg" => "Please enter your email"
       ));
     }
     else if(!filter_var($this->input->post('email'), FILTER_VALIDATE_EMAIL)){
       echo json_encode(array(
         "status" => "ERR",
         "msg" => "Invalid email format"
       ));
     }
     else if ($this->input->post('pic') == NULL){
       echo json_encode(array(
         "status" => "ERR",
         "msg" => "Please input your PIC"
       ));
     }
     else if ($this->input->post('info') == NULL){
       echo json_encode(array(
         "status" => "ERR",
         "msg" => "Please insert info of the company"
       ));
     }

     else if ($this->input->post('ssm') == NULL){
       echo json_encode(array(
         "status" => "ERR",
         "msg" => "Please insert SSM regustration number"
       ));
     }

     else if ($this->input->post('addr') == NULL){
       echo json_encode(array(
         "status" => "ERR",
         "msg" => "Please insert the address"
       ));
     }

     else if ($this->input->post('tel') == NULL){
       echo json_encode(array(
         "status" => "ERR",
         "msg" => "Please insert Company number"
       ));
     }

     else if (!filter_var($this->input->post('tel'), FILTER_SANITIZE_NUMBER_INT)){
       echo json_encode(array(
         "status" => "ERR",
         "msg" => "Invalid number format"
       ));
     }
     else {
       $session_data = $this->session->userdata('logged_in');
       $data['username'] = $session_data['username'];

       $id = '';
       $gen = range(0, 9);
       for ($i = 0; $i < 5; $i++) {
         $id .= $gen[array_rand($gen)];
       }
       $rid = 'VEN'.$id;
       $data = array(
       'vendorID' => $rid,
       'companyName' => $this->input->post('name'),
       'email' => $this->input->post('email'),
       'address' => $this->input->post('addr'),
       'tel' => $this->input->post('tel'),
       'pic' => $this->input->post('pic'),
       'info' => $this->input->post('info'),
       'ssmNo' => $this->input->post('ssm'),
       'issued' => $data['username'],
       );

       $data = $this->security->xss_clean($data);
       if($this->Supplier->saveSup($data) == true) // call the method from the controller
         {
             // update successful...
             echo json_encode(array("status" => 'SUCCESS'));
         }
         else
         {
             echo json_encode(array("status" => 'FAILED'));
         }
     }
   }
   else {
     header("HTTP/1.1 404 Not Found");
     echo "404 not found";
   }


 }

 public function detail($id)
 {
   if($this->session->userdata('logged_in')){
     $data = $this->Supplier->get_by_id($id);
     echo json_encode($data);
   }else {
     header("HTTP/1.1 404 Not Found");
     echo "404 not found";
   }

  }

 function update(){

   if($this->session->userdata('logged_in')){

     if ($this->input->post('name') == NULL){
      echo json_encode(array(
        "status" => "ERR",
        "msg" => "Please enter company name"
      ));
    }
    else if ($this->input->post('email') == NULL){
      echo json_encode(array(
        "status" => "ERR",
        "msg" => "Please enter your email"
      ));
    }
    else if(!filter_var($this->input->post('email'), FILTER_VALIDATE_EMAIL)){
      echo json_encode(array(
        "status" => "ERR",
        "msg" => "Invalid email format"
      ));
    }
    else if ($this->input->post('pic') == NULL){
      echo json_encode(array(
        "status" => "ERR",
        "msg" => "Please input your PIC"
      ));
    }
    else if ($this->input->post('info') == NULL){
      echo json_encode(array(
        "status" => "ERR",
        "msg" => "Please insert info of the company"
      ));
    }

    else if ($this->input->post('ssm') == NULL){
      echo json_encode(array(
        "status" => "ERR",
        "msg" => "Please insert SSM regustration number"
      ));
    }

    else if ($this->input->post('addr') == NULL){
      echo json_encode(array(
        "status" => "ERR",
        "msg" => "Please insert the address"
      ));
    }

    else if ($this->input->post('tel') == NULL){
      echo json_encode(array(
        "status" => "ERR",
        "msg" => "Please insert Company number"
      ));
    }

    else if (!filter_var($this->input->post('tel'), FILTER_SANITIZE_NUMBER_INT)){
      echo json_encode(array(
        "status" => "ERR",
        "msg" => "Invalid number format"
      ));
    }
    else {
     $data = array(
       'id' => $this->input->post('id'),
       'companyName' => $this->input->post('name'),
       'email' => $this->input->post('email'),
       'address' => $this->input->post('addr'),
       'tel' => $this->input->post('tel'),
       'pic' => $this->input->post('pic'),
       'info' => $this->input->post('info'),
       'ssmNo' => $this->input->post('ssm'),
       );

       $data = $this->security->xss_clean($data);
       if($this->Supplier->updateCom($data)) // call the method from the controller
         {
             // update successful...
             echo json_encode(array("status" => 'SUCCESS'));
         }
         else
         {
             echo json_encode(array("status" => 'FAILED'));
         }
       }
   }
   else
   {
     header("HTTP/1.1 404 Not Found");
     echo "404 not found";
   }


     }
}

?>
