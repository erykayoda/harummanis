<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class ManageOrder extends CI_Controller {

 function __construct()
 {
   parent::__construct();
	 $this->load->model('Order','',TRUE);
   $this->load->library('pdf');

 }

 function index()
 {
   if($this->session->userdata('logged_in'))
   {
     $session_data = $this->session->userdata('logged_in');
     $data['username'] = $session_data['username'];
     $this->load->view('admin/manageOrder', $data);
   }
   else
   {
     //If no session, redirect to login page
     redirect('admin/login', 'refresh');
   }
 }

 function loadData(){
   if($this->session->userdata('logged_in'))
   {

     function checkStatus(&$fck){
     	if ($fck == 'ERROR'){
     		 return '<span class="label label-danger">ERROR</span>';
     	}
     	else if($fck == 'NORES') {
     		 return '<span class="label label-danger">No payment information recieved</span>';
     	}
     	else if($fck == 'DELIVERED') {
     		 return '<span class="label label-success">DELIVERED</span>';
     	}
     	else if($fck == 'APPROVE') {
     		 return '<span class="label label-success">APPROVED</span>';
     	}
     	else if($fck == 'FAILED') {
     		 return '<span class="label label-success">FAILED</span>';
     	}
     	else if($fck == 'PENDING'){
     		return '<span class="label label-warning">Your order are being process...</span>';
     	}
     	else if($fck == 'CHECK'){
     		return '<span class="label label-warning">RECEIPT RECIEVED</span>';
     	}
    }

	 $list = $this->Order->getData();
	 //$data = array();
	 foreach ($list as $get) {
            //$no++;
            $data[] = array(
              'id' => $get->orderid,
              'userid' => $get->userid,
              'name' => $get->uname,
              'del' => $get->del_mthd,
              'addr' => $get->addr,
              'date' => $get->date,
              'remarks' => $get->orderid,
              'total' => '<b>RM '.number_format($get->total,2).'</b>',
              'status' => checkStatus($get->status),
              'action' => '<a class="btn btn-sm btn-primary" href="'.base_url().'admin/ManageOrder/detail/'.$get->orderid.'" title="Edit"><i class="fa fa-info" aria-hidden="true"></i> More details</a>'
            );
        }


        //output to json format
        echo json_encode($data);
      }
        else
        {
          //If no session, redirect to login page
          redirect('admin/login', 'refresh');
        }

 }

 public function view($id)
 {

   if($this->session->userdata('logged_in'))
   {
     $far = $this->Order->vieworr($id);
     foreach ($far as $get) {
       $data[] = array(
         'id' => $get->orderid,
         'userid' => $get->userid,
         'name' => $get->name,
         'phone' => $get->phone,
         'del' => $get->del_mthd,
         'addr' => $get->addr,
         'type' => $get->type,
         'qty' => $get->qty,
         'status' => $get->status,
         'date' => $get->date,
         'email' => $get->email,
         'remarks' => $get->remarks,
         'total' => '<b>RM '.number_format($get->total,2).'</b>',
       );
     }
      echo json_encode($data);
   }

     else
     {
       //If no session, redirect to login page
       redirect('admin/login', 'refresh');
     }

  }


  public function ordData($id)
  {
    $inf = $this->Order->orderInf($id);
    foreach ($inf as $get) {
      $data[] = array(
        'brand' => $get->name,
        'id' => $get->proid,
        'price' => '<b>RM '.number_format($get->sprice,2).'</b>',
        'total' => '<b>RM '.number_format(($get->sprice * $get->qty),2).'</b>',
        'qty' => $get->qty,
      );
    }
     echo json_encode($data);
   }




 public function detail($id)
 {
   if($this->session->userdata('logged_in'))
   {
     if(isset($id)){

       $session_data = $this->session->userdata('logged_in');
       $info = $this->Order->vieworr($id);
       $list = $this->Order->orderInf($id);
       $kira = count($list);
       if($kira < 1){
         redirect('admin/manageOrder', 'refresh');
       }
       $gmbar = $this->Order->resit_img($id);

       $data = array(
         'username' => $session_data['username'],
         'details' => $info,
         'resit' => $gmbar,
         'list' => $list, 
         'item_total' => '0'

       );

       $this->load->view('admin/orderDetails' , $data);
     }else {
       redirect('admin/manageOrder', 'refresh');
     }

   }
   else
   {
     //If no session, redirect to login page
     redirect('admin/login', 'refresh');
   }

   //$data = $this->Order->get_by_id($id);
   //echo json_encode($data);
 }



 function deleteOrder(){
  $id = $this->input->post('id');
  if($this->Order->deleteInfo($id)){
    return true;
 }
 else {
   return false;
 }
}

function updateOrder()
{
  $data = array('table' => 'orders',
  'id' => $this->input->post('id'),
  'stat' => $this->input->post('stat')
  );

  $this->load->model('Order'); // First load the model
  //$this->Order->update($data);
  if($this->Order->update($data)) // call the method from the controller
    {
        // update successful...
        echo json_encode(array("status" => 'SUCCESS'));
    }
    else
    {
        echo json_encode(array("status" => 'FAILED'));
    }


 }

 function genpdf(){

   if($this->session->userdata('logged_in'))
   {
   $id = $this->input->post('id');
   $far = $this->Order->vieworr($id);
     $data = array(
       'loop' => $this->Order->printInv($id),
       'detail' => $far,
       'total' => '0'
     );
   $session_data = $this->session->userdata('logged_in');
   //$data['username'] = $session_data['username'];
   $this->load->library('pdf');
   $this->pdf->load_view('admin/invoice' , $data);
   if($this->pdf->load_view('admin/invoice' , $data)){
     echo "OK";
   }
   else {
    echo "DIE";
   }
    }

  else
  {
    //If no session, redirect to login page
    redirect('admin/login', 'refresh');
  }
}


}

?>
