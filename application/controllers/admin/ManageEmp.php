<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class ManageEmp extends CI_Controller {

 function __construct()
 {
   parent::__construct();
	 $this->load->model('Product','',TRUE);
   $this->load->model('Expense','',TRUE);

 }

 function index()
 {
   if($this->session->userdata('logged_in'))
   {
     $session_data = $this->session->userdata('logged_in');
     $data  = array(
       'level' => $session_data['level'],
       'username' => $session_data['username'],
       'list' => $this->Expense->typeList(),
       'company' => $this->Expense->comList()
     );
     //$data['username'] = $session_data['username'];
     $this->load->view('admin/manageEmp', $data);
   }
   else
   {
     //If no session, redirect to login page
     redirect('admin/login', 'refresh');
   }
 }

function loadData(){
  if($this->session->userdata('logged_in'))
  {

  function checkStatus(&$fck){
    if ($fck == '0'){
      return '<span class="label label-danger">UNPAID</span>';
    }
    else if($fck == '1') {
      return '<span class="label label-success">PAID</span>';
    }
  }
  function negaval($hai){
    if ($hai < 0){
      return '<span class="label label-danger">'.$hai.'</span>';
    }
    else {
      return $hai;
    }
  }
  $list = $this->Expense->getData();
	//$data = array();
	 foreach ($list as $get) {
     $data[] = array(
       'id' => $get->expid,
       'venid' => $get->vendorID,
       'name' => $get->name,
       'type' => $get->typeName,
       'issue' => $get->add_by,
       'total' => 'RM ' . number_format($get->total,2),
       'bila' => $get->date_create,
       'remarks' => $get->remarks,
       'status' => checkStatus($get->stat),
       'action' => '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="viewExp('."'".$get->expid."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
             <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="deleteExp('."'".$get->expid."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>'
     );
   }
   echo json_encode($data);
 }

  {
   //If no session, redirect to login page
   redirect('login', 'refresh');
  }
 }

 function addType(){

   $data = array(
     'typeName' => $this->input->post('name'),
     'info' => $this->input->post('info'),
   );
   $data = $this->security->xss_clean($data);
   $this->load->model('Expense');
   if(!$this->Expense->typeAdd($data)) // call the method from the controller
   {
         // update successful...
         echo json_encode(array("status" => 'FAILED'));
    }
    else
     {
       echo json_encode(array("status" => 'SUCCESS'));

     }
 }

function updateExpense()
{
   $data = array(
     'expid' => $this->input->post('id'),
     'vendorID' => $this->input->post('ven'),
     'name' => $this->input->post('expname'),
     'total' => $this->input->post('total'),
     'remarks' => $this->input->post('info'),
     'stat' => $this->input->post('stat'),
     'typID' => $this->input->post('typ'),
     'total' => $this->input->post('total'),
   );

   $data = $this->security->xss_clean($data);
   if($this->Expense->updateExp($data)) // call the method from the controller
    {
         // update successful...
         echo json_encode(array("status" => 'SUCCESS'));
     }
     else
     {
         echo json_encode(array("status" => 'FAILED'));
     }
}


function add(){

  $session_data = $this->session->userdata('logged_in');
  $data['username'] = $session_data['username'];

  $id = '';
  $gen = range(0, 9);
  for ($i = 0; $i < 5; $i++) {
    $id .= $gen[array_rand($gen)];
  }
  $rid = 'EXP'.$id;
  $data = array(
  'expid' => $rid,
  'vendorID' => $this->input->post('ven'),
  'name' => $this->input->post('expname'),
  'total' => $this->input->post('total'),
  'remarks' => $this->input->post('info'),
  'stat' => $this->input->post('stat'),
  'typID' => $this->input->post('typ'),
  'total' => $this->input->post('total'),
  'add_by' => $data['username'],
  );
  $data = $this->security->xss_clean($data);
  if($this->Expense->saveExp($data)) // call the method from the controller
    {
        // update successful...
        echo json_encode(array("status" => 'SUCCESS'));
    }
    else
    {
        echo json_encode(array("status" => 'FAILED'));
    }

}

public function detail($id)
{
  $data = $this->Expense->getInfo($id);
  echo json_encode($data);
}

 function delete($id){
   $data = $this->Expense->deleteInfo($id);
   echo json_encode($data);
 }




}

?>
