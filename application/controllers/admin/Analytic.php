<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class analytic extends CI_Controller {

 function __construct()
 {
   parent::__construct();
   $this->load->model('Anal','',TRUE);
	 //$this->load->model('Order','',TRUE);
 }

 function index()
 {
   if($this->session->userdata('logged_in'))
   {
     $session_data = $this->session->userdata('logged_in');
     $data  = array(
       'level' => $session_data['level'],
       'username' => $session_data['username'],
       'pending' => $this->Anal->pending(),
       'deliver' => $this->Anal->deliver(),
       'exp' => $this->Anal->expense(),
       'sales' => $this->Anal->sales(),
       'goal' => $this->Anal->modal(),
       'profit' => $this->Anal->sales() - ($this->Anal->expense() + $this->Anal->modal())
     );
     $this->load->view('admin/static', $data);

   }
   else
   {
     redirect('login', 'refresh');
   }
 }

 function loadData(){

	 $list = $this->Order->getData();
	 $data = array();
	 foreach ($list as $person) {
            //$no++;
            $row = array();
            $row[] = $person->proid;
						$row[] = $person->name;
						$row[] = $person->price;

            //add html for action
            $row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_person('."'".$person->proid."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
                  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('."'".$person->proid."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';

            $data[] = $row;
        }


        //output to json format
        echo json_encode($data);

 }

 function deleteOrder($id){
   //fck
 }
 function bulan(){

   header('Content-Type: application/json');

   $list = $this->Anal->bulansales();
   $data_points = array();

   foreach ($list as $row){

     $point = array(
       "label" => $row->bulan ,
       "y" => $row->total
     );

     array_push($data_points, $point);
   }




        //output to json format
        echo json_encode($data_points , JSON_NUMERIC_CHECK);
 }

  function mana(){

    header('Content-Type: application/json');

    $list = $this->Anal->masuk();
    $data_points = array();

 	 foreach ($list as $row){

      $point = array(
        "label" => $row->masuk ,
        "y" => $row->total
      );

      array_push($data_points, $point);
    }




         //output to json format
         echo json_encode($data_points , JSON_NUMERIC_CHECK);
  }


}

?>
