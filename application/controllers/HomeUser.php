<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class HomeUser extends CI_Controller {

 function __construct()
 {
   parent::__construct();
	 $this->load->model('Product','',TRUE);
   $this->load->model('User','',TRUE);
   $this->load->model('Order','',TRUE);
 }

 function index()
 {
   //$this->load->helper('url');
   if($this->session->userdata('user'))
   {
     $session_data = $this->session->userdata('user');
     $id = $session_data['userid'];
     $data  = array(
       'level' => $session_data['level'],
       'username' => $session_data['userid'],
       'id' => $session_data['userid'],
       'order' => $this->User->allorder($id),
       'barang' => $this->User->listBrg()
       //'order' => $this->Order->get_by_id($session_data['userid']),
     );
     $this->load->view('home_user',$data);
   }
   else
   {
     //If no session, redirect to login page
     redirect('login', 'refresh');
   }
 }

function myOrder(){

  $session_data = $this->session->userdata('user');
  $id = $session_data['userid'];
  $info  = $this->Product->allorder($id);
  foreach ($info as $get) {
    $data[] = array(
      'id' => $get->orderid,
      'userid' => $get->userid,
      'name' => $get->name,
      'del' => $get->del_mthd,
      'addr' => $get->addr,
      'type' => $get->type,
      'qty' => $get->qty,
      'status' => $get->status,
      'date' => $get->date,
      'email' => $get->email,
      'remarks' => $get->remarks,
      'total' => '<b>RM '.number_format($get->total,2).'</b>',
    );
  }
   echo json_encode($data);

}
function userDetail($id){

  $data  = $this->User->getUser($id);
  echo json_encode($data);
}

function updateOrder(){

  $session_data = $this->session->userdata('user');
  //$uid = $session_data['userid'];
  $id = '';
  $gen = range(0, 9);
  for ($i = 0; $i < 3; $i++) {
    $id .= $gen[array_rand($gen)];
  }
  $rid = 'res'.$id;
    $status = "";
    $msg = "";
    $file_element_name = 'userfile';

    if ($status != "error")
    {
        $config['upload_path'] = '/var/www/html/harum/files';
        $config['allowed_types'] = 'jpeg|png';
        $config['max_size'] = 1024 * 8;
        $config['encrypt_name'] = FALSE;
        $config['file_name'] = $rid;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload($file_element_name))
        {
            $status = 'error';
            $msg = $this->upload->display_errors('', '');
        }
        else
        {
            $go = $this->upload->data();

            $data = array(
              'imgid' =>  $this->upload->data('file_name'),
              'orderid' => $this->input->post('orderid'),
            );

            $data1 = array(
              'status' =>  'CHECK',
              'orderid' => $this->input->post('orderid'),
            );

            $file_id = $this->User->push_resit($data);
            $update = $this->User->updateStat($data1);
            if($file_id == TRUE)
            {
                $status = "success";
                $msg = "File successfully uploaded";
            }
            else
            {
                unlink($go['full_path']);
                $status = "error";
                $msg = "Something went wrong when saving the file, please try again.";
            }
        }
        @unlink($_FILES[$file_element_name]);
    }
    echo json_encode(array("status" => $status, "msg" => $msg));
}


function addorder()
{

  if($this->session->userdata('user'))
  {
    //ajax manual validation check
    if($this->input->post('qty') <= 0){
      echo json_encode(array(
        "status" => "ERR",
        "msg" => "Your quantity must more than 0"
      ));
    }
    else if(!filter_var($this->input->post('qty'), FILTER_VALIDATE_INT)){
      echo json_encode(array(
        "status" => "ERR",
        "msg" => "Only number allowed!"
      ));
    }
    else if ($this->input->post('name') == NULL){
      echo json_encode(array(
        "status" => "ERR",
        "msg" => "Please enter your name"
      ));
    }
    else if ($this->input->post('email') == NULL){
      echo json_encode(array(
        "status" => "ERR",
        "msg" => "Please enter your email"
      ));
    }

    else if(!filter_var($this->input->post('email'), FILTER_VALIDATE_EMAIL)){
      echo json_encode(array(
        "status" => "ERR",
        "msg" => "Invalid email format"
      ));
    }

    else if ($this->input->post('addr') == NULL){
      echo json_encode(array(
        "status" => "ERR",
        "msg" => "Please input your address"
      ));
    }
    else if ($this->input->post('mthd') == NULL){
      echo json_encode(array(
        "status" => "ERR",
        "msg" => "Invalid delivery method!"
      ));
    }
    else
    {

      $session_data = $this->session->userdata('user');
      $uid = $session_data['userid'];
      $id = '';
      $gen = range(0, 9);
      for ($i = 0; $i < 5; $i++) {
        $id .= $gen[array_rand($gen)];
      }
      $rid = 'HRM'.$id;
      $iid = $this->input->post('typitem');
      $item = $this->User->getInfo($iid);
      foreach($item as $itm)
      {
        $sell = $itm['sprice'];
        $name = $itm['name'];
        $type = $itm['type'];
      }
      $ttl = $sell * $this->input->post('qty');
      $data1 = array(
        'proid' => $iid,
        'orderid' => $rid,
        'type' => $type,
        'qty' => $this->input->post('qty'),
      );
      $insert1 = $this->Order->listorder($data1);
      $data = array(
        'del_mthd' => $this->input->post('mthd'),
        'userid' => $uid,
        'remarks' => $this->input->post('remarks'),
        'masuk' => 'WEB',
        'total' => $ttl,
        'status' => 'NORES',
        'orderid' => $rid,
      );

      $insert = $this->Order->save($data);
      echo json_encode(array("status" => TRUE));
  }

  }
  else {
    header("HTTP/1.1 404 Not Found");
    echo "404 not found";
  }

}


public function view($id)
{
  $data = $this->User->test($id);
  echo json_encode($data);
}

function delete($id){
  $data = $this->User->deleteOdor($id);
  echo json_encode("OK");
}


 function logout()
 {
   $this->session->unset_userdata('user');
   session_destroy();
   redirect('HomeUser', 'refresh');
 }

}

?>
