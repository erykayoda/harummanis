<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class UserProfile extends CI_Controller {

 function __construct()
 {
   parent::__construct();
	 $this->load->model('Order','',TRUE);
 }

 function index()
 {
   if($this->session->userdata('user'))
   {
     $session_data = $this->session->userdata('user');
     $data['username'] = $session_data['username'];
     $this->load->view('manageOrder', $data);
   }
   else
   {
     //If no session, redirect to login page
     redirect('login', 'refresh');
   }
 }

 function loadData(){

	 $list = $this->Order->getData();
	 //$data = array();
	 foreach ($list as $get) {
            //$no++;
            $data[] = array(
              'id' => $get->orderid,
              'name' => $get->name,
              'del' => $get->del_mthd,
              'addr' => $get->addr,
              'type' => $get->type,
              'qty' => $get->qty,
              'date' => $get->date,
              'remarks' => $get->orderid,
              'total' => $get->total,
              'status' => $get->status,
              'action' => '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_person('."'".$get->orderid."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
                    <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('."'".$get->orderid."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>'
            );
        }


        //output to json format
        echo json_encode($data);

 }

 public function detail($id)
    {
        $data = $this->Order->get_by_id($id);
        //$data->dob = ($data->dob == '0000-00-00') ? '' : $data->dob; // if 0000-00-00 set tu empty for datepicker compatibility
        echo json_encode($data);
    }

 function deleteOrder($id){
   //fck
 }



}

?>
