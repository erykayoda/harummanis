<?php
//require('/var/www/html/ors/dataportal/config/functions.php');
if (isset($this->session->userdata['logged_in'])) {
$username = ($this->session->userdata['logged_in']['username']);
//$email = ($this->session->userdata['logged_in']['email']);
} else {
header("location:admin/login");
}
//displayValue();
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>HarumManis - Manage Order</title>

<link href="<?php echo base_url()?>//asset/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url()?>//asset/css/datepicker3.css" rel="stylesheet">
<link href="<?php echo base_url()?>//asset/css/bootstrap-table.css" rel="stylesheet">
<link href="<?php echo base_url()?>//asset/css/styles.css" rel="stylesheet">
<link href="<?php echo base_url()?>//asset/css/font-awesome.min.css" rel="stylesheet">

<!--Icons-->
<script src="<?php echo base_url()?>//asset/js/lumino.glyphs.js"></script>

<!--[if lt IE 9]>
<script src="/asset/js/html5shiv.js"></script>
<script src="/asset/js/respond.min.js"></script>
<![endif]-->

</head>

<body>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"><span></span>HarumManis - BMS</a>
				<ul class="user-menu">
					<li class="dropdown pull-right">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>Welcome	 , <?php echo $username ?> <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> Profile</a></li>
							<li><a href="#"><svg class="glyph stroked gear"><use xlink:href="#stroked-gear"></use></svg> Settings</a></li>
							<li><a href="<?php echo site_url() ?>admin/logout"><svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Logout</a></li>
						</ul>
					</li>
				</ul>
			</div>

		</div><!-- /.container-fluid -->
	</nav>

  <div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form>
		<ul class="nav menu">
			<li><a href="<?php echo site_url() ?>admin/home"><svg class="glyph stroked dashboard-dial"><use xlink:href="#stroked-dashboard-dial"></use></svg> Dashboard</a></li>
			<li><a href="<?php echo site_url() ?>admin/analytic"><svg class="glyph stroked line-graph"><use xlink:href="#stroked-line-graph"></use></svg> Data Analytics</a></li>
			<li><a href="<?php echo site_url() ?>admin/ManageOrder"><svg class="glyph stroked table"><use xlink:href="#stroked-table"></use></svg> Manage Order</a></li>
			<li><a href="<?php echo site_url() ?>admin/ManagePro"><svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"/></svg> Manage Product</a></li>
			<li><a href="<?php echo site_url() ?>admin/ManageSpp"><svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"/></svg> Manage Supplier</a></li>
			<li class="active"><a href="<?php echo site_url() ?>admin/ManageExp"><svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"/></svg> Manage Expense</a></li>
			<li><a href="<?php echo site_url() ?>admin/ManageEmp"><svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"/></svg> Manage Employee</a></li>
		</ul>
	</div><!--/.sidebar-->

	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="index.php"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
				<li class="active">Manage Expenses</li>
			</ol>
		</div><!--/.row-->

		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header"></h1>
			</div>
		</div><!--/.row-->

		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-12">
					<a class="btn btn-success" href="javascript:void(0)" onclick="addExp();"><i class="fa fa-plus" aria-hidden="true"></i>  Add new expense</a>
					<a class="btn btn-danger" href="javascript:void(0)" onclick="expType();"><i class="fa fa-plus" aria-hidden="true"></i>  Add expense	 type</a>
				</div>
				</div>
				<hr>
				<div class="panel panel-primary">
					<div class="panel-heading">Expense</div>
					<div class="panel-body">
						<ul class="nav nav-tabs">
								<li class="active"><a href="#tab1" data-toggle="tab">Expense List</a></li>
								<li><a href="#tab2" data-toggle="tab">Expense type</a></li>
						</ul>
					</div>
					<div class="panel-body">
							<div class="tab-content">
									<div class="tab-pane fade in active" id="tab1">
										<table  data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc" id='data'>
									    <thead>
									    <tr>
									        <th data-field="id" data-sortable="true" >ID</th>
									        <th data-field="name" data-sortable="true">Name</th>
									        <th data-field="bila"  data-sortable="true">Date Created</th>
													<th data-field="type"  data-sortable="true">Type</th>
													<th data-field="total"  data-sortable="true">Total Price</th>
													<th data-field="status"  data-sortable="true">Status</th>
													<th data-field="issue"  data-sortable="true">Issue By</th>
			                    <th data-field="action"  data-sortable="true">Action</th>
									    </tr>
									    </thead>

									</table>
									</div>
									<div class="tab-pane fade" id="tab2">
										<table  data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc" id='typeexp'>
									    <thead>
									    <tr>
									        <th data-field="id" data-sortable="true" >ID</th>
									        <th data-field="name" data-sortable="true">Name</th>
									        <th data-field="info"  data-sortable="true">Description</th>
													<th data-field="issue"  data-sortable="true">Issue By</th>
			                    <th data-field="action"  data-sortable="true">Action</th>
									    </tr>
									    </thead>

									</table>
								</div>
							</div>
					</div>
				</div>


			</div>
		</div><!--/.row-->



	</div><!--/.main-->

	<script src="<?php echo base_url()?>//asset/js/jquery-1.11.1.min.js"></script>
	<script src="<?php echo base_url()?>//asset/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url()?>//asset/js/bootstrap-datepicker.js"></script>
	<script src="<?php echo base_url()?>//asset/js/bootstrap-table.js"></script>
	<script src="<?php echo base_url()?>//asset/js/admin.js"></script>

<!-- Bootstrap modal -->
<div class="modal fade" id="addtype" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title"></h3>
            </div>
						<div class="modal-body form">
                <form action="#" id="addExpType" class="form-horizontal">
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
                    <input type="hidden" value="" name="id"/>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">expense type:</label>
                            <div class="col-md-9">
                                <input name="name" placeholder="Type name" class="form-control" type="text" required="true">
                                <span class="help-block"></span>
                            </div>
                        </div>
												<div class="form-group">
                            <label class="control-label col-md-3">Remarks:</label>
                            <div class="col-md-9">
                                <textarea name="info" placeholder="remarks" class="form-control" type="text" required="true"></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="saveType();" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i>  Add</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title"></h3>
            </div>
						<div class="modal-body form">
                <form action="#" id="addExpd" class="form-horizontal">
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
                    <input type="hidden" value="" name="id"/>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Expense name:</label>
                            <div class="col-md-9">
                                <input name="expname" placeholder="Expense name" class="form-control" type="text" required="true">
                                <span class="help-block"></span>
                            </div>
                        </div>
												<div class="form-group">
                            <label class="control-label col-md-3">Company name:</label>
                            <div class="col-md-9">
															<select name="ven" id="ven" class="form-control">
																<?php foreach($company as $lisst) { ?>
															<option value="<?php echo $lisst['vendorID'] ?>"><?php echo $lisst['companyName'] ?></option>
															<?php } ?>
															</select>
                                <span class="help-block"></span>
                            </div>
                        </div>
												<div class="form-group">
                            <label class="control-label col-md-3">Expense type:</label>
                            <div class="col-md-9">
                                <select name="typ" id="typ" class="form-control">
																	<?php foreach($list as $lisst) { ?>
																<option value="<?php echo $lisst['typID'] ?>"><?php echo $lisst['typeName'] ?></option>
																<?php } ?>
																</select>
                                <span class="help-block"></span>
                            </div>
                        </div>
												<div class="form-group">
                            <label class="control-label col-md-3">Total: (<b>RM</b>)</label>
                            <div class="col-md-9">
                                <input name="total" placeholder="Total" class="form-control" type="text" required="true">
                                <span class="help-block"></span>
                            </div>
                        </div>

												<div class="form-group">
                            <label class="control-label col-md-3">Remarks</label>
                            <div class="col-md-9">
                                <textarea name="info" placeholder="Payment remarks" class="form-control" type="text" required="true"></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>
												<div class="form-group">
                            <label class="control-label col-md-3">Status</label>
                            <div class="col-md-9">
                                <select name="stat" id="stat" class="form-control">
																	<option value="1">PAID</option>
																	<option value="0">UNPAID</option>
																</select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnAdd" onclick="addExpense()" class="btn btn-primary">Add Expense</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="exp_view" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title"></h3>
            </div>
						<div class="modal-body form">
                <form action="#" id="expview" class="form-horizontal">
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
                    <input type="hidden" value="" name="id"/>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Expense name:</label>
                            <div class="col-md-9">
                                <input name="expname" placeholder="Expense name" class="form-control" type="text" required="true">
                                <span class="help-block"></span>
                            </div>
                        </div>
												<div class="form-group">
                            <label class="control-label col-md-3">Company name:</label>
                            <div class="col-md-9">
															<select name="ven" id="ven" class="form-control">
																<?php foreach($company as $lisst) { ?>
															<option value="<?php echo $lisst['vendorID'] ?>"><?php echo $lisst['companyName'] ?></option>
															<?php } ?>
															</select>
                                <span class="help-block"></span>
                            </div>
                        </div>
												<div class="form-group">
                            <label class="control-label col-md-3">Expense type:</label>
                            <div class="col-md-9">
                                <select name="typ" id="typ" class="form-control">
																	<?php foreach($list as $lisst) { ?>
																<option value="<?php echo $lisst['typID'] ?>"><?php echo $lisst['typeName'] ?></option>
																<?php } ?>
																</select>
                                <span class="help-block"></span>
                            </div>
                        </div>
												<div class="form-group">
                            <label class="control-label col-md-3">Total: (<b>RM</b>)</label>
                            <div class="col-md-9">
                                <input name="total" placeholder="Total" class="form-control" type="text" required="true">
                                <span class="help-block"></span>
                            </div>
                        </div>

												<div class="form-group">
                            <label class="control-label col-md-3">Remarks</label>
                            <div class="col-md-9">
                                <textarea name="info" placeholder="Payment remarks" class="form-control" type="text" required="true"></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>
												<div class="form-group">
                            <label class="control-label col-md-3">Status</label>
                            <div class="col-md-9">
                                <select name="stat" id="stat" class="form-control">
																	<option value="1">PAID</option>
																	<option value="0">UNPAID</option>
																</select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnUpd" onclick="updateExp()" class="btn btn-primary">Update</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- End Bootstrap modal -->
<script src="<?php echo base_url()?>//asset/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url()?>//asset/js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>//asset/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url()?>//asset/js/bootstrap-table.js"></script>
<script src="<?php echo base_url()?>//asset/js/admin.js"></script>
<script src="<?php echo base_url()?>//asset/js/js-cookie.js"></script>
	<script>
		!function ($) {
			$(document).on("click","ul.nav li.parent > a > span.icon", function(){
				$(this).find('em:first').toggleClass("glyphicon-minus");
			});
			$(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})

		$(document).ready(function(){
			go();
			var token_id = Cookies.get('harum_cookie');
	    var token = {
	      secure_token : token_id,
	    }
			$.ajax({
	      url: window.location.href + "/type/",
	      type:'GET',
				dataType:'JSON',
	      success:function(data)
	      {
					console.log(data);
					$("#typeexp").bootstrapTable({
							data:data
						})
	      }
	    })
		});
	</script>
</body>

</html>
