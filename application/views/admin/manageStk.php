<?php
//require('/var/www/html/ors/dataportal/config/functions.php');
if (isset($this->session->userdata['logged_in'])) {
$username = ($this->session->userdata['logged_in']['username']);
//$email = ($this->session->userdata['logged_in']['email']);
} else {
header("location:admin/login");
}
//displayValue();
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>HarumManis - Manage Supplier</title>

<link href="<?php echo base_url()?>//asset/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url()?>//asset/css/datepicker3.css" rel="stylesheet">
<link href="<?php echo base_url()?>//asset/css/bootstrap-table.css" rel="stylesheet">
<link href="<?php echo base_url()?>//asset/css/styles.css" rel="stylesheet">
<link href="<?php echo base_url()?>//asset/css/font-awesome.min.css" rel="stylesheet">

<!--Icons-->
<script src="<?php echo base_url()?>//asset/js/lumino.glyphs.js"></script>

<!--[if lt IE 9]>
<script src="/asset/js/html5shiv.js"></script>
<script src="/asset/js/respond.min.js"></script>
<![endif]-->

</head>

<body>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"><span></span>HarumManis - BMS</a>
				<ul class="user-menu">
					<li class="dropdown pull-right">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>Welcome	 , <?php echo $username ?> <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> Profile</a></li>
							<li><a href="#"><svg class="glyph stroked gear"><use xlink:href="#stroked-gear"></use></svg> Settings</a></li>
							<li><a href="<?php echo site_url() ?>admin/logout"><svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Logout</a></li>
						</ul>
					</li>
				</ul>
			</div>

		</div><!-- /.container-fluid -->
	</nav>

  <div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form>
		<ul class="nav menu">
			<li><a href="<?php echo site_url() ?>admin/home"><svg class="glyph stroked dashboard-dial"><use xlink:href="#stroked-dashboard-dial"></use></svg> Dashboard</a></li>
			<li><a href="<?php echo site_url() ?>admin/analytic"><svg class="glyph stroked line-graph"><use xlink:href="#stroked-line-graph"></use></svg> Data Analytics</a></li>
			<li><a href="<?php echo site_url() ?>admin/ManageOrder"><svg class="glyph stroked table"><use xlink:href="#stroked-table"></use></svg> Manage Order</a></li>
			<li><a href="<?php echo site_url() ?>admin/ManagePro"><svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"/></svg> Manage Product</a></li>
			<li class="active"><a href="<?php echo site_url() ?>admin/ManageSpp"><svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"/></svg> Manage Supplier</a></li>
			<li><a href="<?php echo site_url() ?>admin/ManageExp"><svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"/></svg> Manage Expense</a></li>
			<li><a href="<?php echo site_url() ?>admin/ManageEmp"><svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"/></svg> Manage Employee</a></li>
		</ul>
	</div><!--/.sidebar-->

	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="index.php"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
				<li class="active">Manage Supplier</li>
			</ol>
		</div><!--/.row-->

		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header"></h1>
			</div>
		</div><!--/.row-->



		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-12">
					<a class="btn btn-primary" href="javascript:void(0)" title="Edit" onclick="addCom();"><i class="fa fa-plus" aria-hidden="true"></i>  Add Supplier</a>
				</div>
				</div>
				<hr>
				<div class="panel panel-primary">
					<div class="panel-heading">Supplier List</div>
					<div class="panel-body">
						<table  data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc" id='data'>
						    <thead>
						    <tr>
						        <th data-field="id" data-sortable="true" >Supplier ID</th>
						        <th data-field="ssm" data-sortable="true">SSM number</th>
						        <th data-field="name"  data-sortable="true">Company name</th>
										<th data-field="date"  data-sortable="true">Date Created</th>
										<th data-field="issue"  data-sortable="true">Issue By</th>
                    <th data-field="action"  data-sortable="true">Info</th>
						    </tr>
						    </thead>

						</table>
					</div>
				</div>
			</div>
		</div><!--/.row-->



	</div><!--/.main-->

<!-- Bootstrap modal -->
<div class="modal fade" id="disCom" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title"></h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="displayCom" class="form-horizontal">
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
                    <input type="hidden" value="" name="id"/>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Company name:</label>
                            <div class="col-md-9">
                                <input name="name" placeholder="Company Name" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
												<div class="form-group">
                            <label class="control-label col-md-3">Company email</label>
                            <div class="col-md-9">
                                <textarea name="email" placeholder="Remarks" class="form-control" type="text"></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>
												<div class="form-group">
                            <label class="control-label col-md-3">Company info</label>
                            <div class="col-md-9">
                                <textarea name="info" placeholder="Remarks" class="form-control" type="text"></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Address:</label>
                            <div class="col-md-9">
                                <textarea name="addr" placeholder="Address" class="form-control" type="text"></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>
												<div class="form-group">
                            <label class="control-label col-md-3">Telephone No:</label>
                            <div class="col-md-9">
                                <input name="tel" placeholder="phone" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
												<div class="form-group">
                            <label class="control-label col-md-3">SSM No:</label>
                            <div class="col-md-9">
                                <input name="ssm" placeholder="SSM No" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
												<div class="form-group">
                            <label class="control-label col-md-3">Person in charge:</label>
                            <div class="col-md-9">
                                <input name="pic" placeholder="Person in charge" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Registered date</label>
                            <div class="col-md-9">
                                <input name="date" placeholder="yyyy-mm-dd" class="form-control datepicker" type="text">
                                <span class="help-block"></span>
                            </div>
														<input type="hidden" value="" name="userid">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="updateCom()" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<div class="modal fade" id="addCompany" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Add new supplier</h3>
            </div>
            <div class="modal-body form">
							<form action="#" id="formCom" class="form-horizontal">
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
									<input type="hidden" value="" name="id"/>
									<div class="form-body">
											<div class="form-group">
													<label class="control-label col-md-3">Company name:</label>
													<div class="col-md-9">
															<input name="name" placeholder="Company Name" class="form-control" type="text">
															<span class="help-block"></span>
													</div>
											</div>
											<div class="form-group">
													<label class="control-label col-md-3">Company email</label>
													<div class="col-md-9">
															<input name="email" placeholder="email" class="form-control" type="email">
															<span class="help-block"></span>
													</div>
											</div>
											<div class="form-group">
													<label class="control-label col-md-3">Company info</label>
													<div class="col-md-9">
															<textarea name="info" placeholder="Remarks" class="form-control" type="text"></textarea>
															<span class="help-block"></span>
													</div>
											</div>
											<div class="form-group">
													<label class="control-label col-md-3">Address:</label>
													<div class="col-md-9">
															<textarea name="addr" placeholder="Address" class="form-control" type="text"></textarea>
															<span class="help-block"></span>
													</div>
											</div>
											<div class="form-group">
													<label class="control-label col-md-3">Telephone No:</label>
													<div class="col-md-9">
															<input name="tel" placeholder="phone" class="form-control" type="text">
															<span class="help-block"></span>
													</div>
											</div>
											<div class="form-group">
													<label class="control-label col-md-3">SSM No:</label>
													<div class="col-md-9">
															<input name="ssm" placeholder="SSM No" class="form-control" type="text">
															<span class="help-block"></span>
													</div>
											</div>
											<div class="form-group">
													<label class="control-label col-md-3">Person in charge:</label>
													<div class="col-md-9">
															<input name="pic" placeholder="Person in charge" class="form-control" type="text">
															<span class="help-block"></span>
													</div>
											</div>
									</div>
							</form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="saveCom()" class="btn btn-primary">Add Company</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<script src="<?php echo base_url()?>//asset/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url()?>//asset/js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>//asset/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url()?>//asset/js/bootstrap-table.js"></script>
<script src="<?php echo base_url()?>//asset/js/admin.js"></script>
<script src="<?php echo base_url()?>//asset/js/js-cookie.js"></script>
<!-- /.modal -->
<!-- End Bootstrap modal -->
	<script>
		!function ($) {
			$(document).on("click","ul.nav li.parent > a > span.icon", function(){
				$(this).find('em:first').toggleClass("glyphicon-minus");
			});
			$(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})
		$(document).ready(function(){
			go();
		});
	</script>
</body>


</html>
