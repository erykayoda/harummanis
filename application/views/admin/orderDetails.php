<?php
//require('/var/www/html/ors/dataportal/config/functions.php');
if (isset($this->session->userdata['logged_in'])) {
$username = ($this->session->userdata['logged_in']['username']);
//$email = ($this->session->userdata['logged_in']['email']);
}

function checkStatus(&$fck){
 if ($fck == 'ERROR'){
		return '<span class="label label-danger">ERROR</span>';
 }
 else if($fck == 'NORES') {
		return '<span class="label label-danger">No payment information recieved</span>';
 }
 else if($fck == 'DELIVERED') {
		return '<span class="label label-success">DELIVERED</span>';
 }
 else if($fck == 'APPROVE') {
		return '<span class="label label-success">APPROVED</span>';
 }
 else if($fck == 'FAILED') {
		return '<span class="label label-success">FAILED</span>';
 }
 else if($fck == 'PENDING'){
	 return '<span class="label label-warning">Your order are being process...</span>';
 }
 else if($fck == 'CHECK'){
	 return '<span class="label label-warning">RECEIPT RECIEVED</span>';
 }
}
//displayValue();
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ReportDetail - UnitenORS</title>

<link href="<?php echo base_url()?>asset/css/font-awesome.min.css" rel="stylesheet">
<link href="<?php echo base_url()?>/asset/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url()?>/asset/css/datepicker3.css" rel="stylesheet">
<link href="<?php echo base_url()?>/asset/css/styles.css" rel="stylesheet">
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>


<!--Icons-->
<script src="<?php echo base_url()?>/asset/js/lumino.glyphs.js"></script>

<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->

</head>

<body>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"><span></span>HarumManis - BMS </a>
				<ul class="user-menu">
					<li class="dropdown pull-right">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>Welcome	 , <?php echo $username ?> <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> Profile</a></li>
							<li><a href="#"><svg class="glyph stroked gear"><use xlink:href="#stroked-gear"></use></svg> Settings</a></li>
						<li><a href="HomeUser/logout"><svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Logout</a></li>
						</ul>
					</li>
				</ul>
			</div>

		</div><!-- /.container-fluid -->
	</nav>

	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form>
		<ul class="nav menu">
			<li><a href="<?php echo site_url() ?>admin/home"><svg class="glyph stroked dashboard-dial"><use xlink:href="#stroked-dashboard-dial"></use></svg> Dashboard</a></li>
			<li><a href="<?php echo site_url() ?>admin/analytic"><svg class="glyph stroked line-graph"><use xlink:href="#stroked-line-graph"></use></svg> Data Analytics</a></li>
			<li class="active"><a href="<?php echo site_url() ?>admin/ManageOrder"><svg class="glyph stroked table"><use xlink:href="#stroked-table"></use></svg> Manage Order</a></li>
			<li><a href="<?php echo site_url() ?>admin/ManagePro"><svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"/></svg> Manage Product</a></li>
			<li><a href="<?php echo site_url() ?>admin/ManageSpp"><svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"/></svg> Manage Supplier</a></li>
			<li><a href="<?php echo site_url() ?>admin/ManageExp"><svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"/></svg> Manage Expense</a></li>
			<li><a href="<?php echo site_url() ?>admin/ManageEmp"><svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"/></svg> Manage Employee</a></li>
		</ul>
	</div><!--/.sidebar-->

	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
				<li class="active">Report Details</li>
			</ol>
    </ul>
		</div><!--/.row-->

		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header"></h1>
			</div>
		</div><!--/.row-->
    <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-primary">
					<?php //foreach($details as $lit) { ?>
					<div class="panel-heading">Order Details for order ID  <?php echo $details['orderid']; ?></div>
							<div class="panel-body">
						<div class="col-md-6">
							<form role="form" action="">
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
								<input type="hidden" id="id" value="<?php echo $details['orderid']; ?>">
							</form>
								<table class="table table-bordered table-hover">
									<tr>
										<th>Email</th>
										<td><?php echo $details['email'] ?></td>
									</tr>
									<tr>
										<th>Customer Name</th>
										<td><?php echo $details['uname'] ?></td>
									</tr>
                  <tr>
										<th>Source:</th>
										<td><?php echo $details['masuk'] ?></td>
									</tr>
									<tr>
										<th>Order status:</th>
										<td><?php echo checkStatus($details['status']) ?></td>
									</tr>
									<tr>
										<th>Date Ordered:</th>
										<td><?php echo $details['date'] ?></td>
									</tr>
									<tr>
										<th>Shipping address:</th>
										<td><?php echo $details['addr'] ?></td>
									</tr>
									<tr>
										<th>Shipping method:</th>
										<td><?php echo $details['del_mthd'] ?></td>
									</tr>
								</table>
								<div class="form-group">
									<label>Change Status</label>
									<select class="form-control" id="status">
										<option value="DELIVERED">DELIVERED</option>
										<option value="PENDING">TRANSACTION FAILED</option>
										<option value="NORES">NO PAYMENT</option>
									</select>
								</div>
								<input id="start" value="<?php echo $details['status'] ?>"  hidden/>
								<button type="button" id="genInc" onclick="gen()" class="btn btn-success"><i class="fa fa-print" aria-hidden="true"></i> Generate Invoice</button>
								<button type="button" id="btn" class="btn btn-primary" onclick="changeStat()"><i class="fa fa-pencil" aria-hidden="true"></i> Change Status</button>
								<button type="button" id="delete" class="btn btn-danger" onclick="deleteOdr('<?php echo $details['orderid'] ?>')"><i class="fa fa-trash" aria-hidden="true"></i> Delete Order</button><br>
								<br><input type="checkbox" id="unlock" name="unlock" onclick="buka()">  Unlock data<br>

							</div>
							<div class="col-md-6">
							<table class="table table-bordered table-hover">
							<tr>
								<th>Receipt Image/Proof</th>
								<td>
									<img src="<?php echo base_url()?>/files/<?php echo $resit['imgid'] ?>" class="img-thumbnail" alt="report image" width="300" height="300">
								</td>
								</tr>
								</table>
              </div>
          </div>
					<?php //} ?>
        </div>
      </div><!-- /.col-->
    </div><!-- /.row -->
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-primary">
					<div class="panel-heading">Item selected</div>
					<div class="panel-body">
						<div class="table-responsive table-bordered">
								<table class="table table-hover">
										<thead>
												<tr>
														<th>Item ID</th>
														<th>Item Name</th>
														<th>Quantity</th>
														<th>Price each</th>
														<th>Total price</th>
												</tr>
										</thead>
										<tbody>
											<?php foreach($list as $lat){ ?>
												<tr>
														<td><?php echo $lat->proid ?></td>
														<td><?php echo $lat->name ?></td>
														<td><?php echo $lat->qty ?></td>
														<td>RM<?php echo number_format($lat->sprice, 2);  ?></td>
														<td>RM<?php echo number_format(($lat->sprice * $lat->qty), 2)  ?></td>
												</tr>
												<?php
												$item_total += ($lat->sprice * $lat->qty );
											} ?>
										</tbody>
										<tfoot>
												<tr>
														<th colspan="4">Total Amount</th>
														<th ><?php echo "RM".number_format($item_total , 2); ?></th>
												</tr>
										</tfoot>
								</table>
						</div>
					</div>
				</div>
				</div>
			</div>

			</div><!--/.row-->


	</div><!--/.main-->

	<div class="modal fade" id="stat" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
         <div class="modal-dialog">
             <div class="modal-content">

                 <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     <h4 class="modal-title" id="myModalLabel">Confirm status change</h4>
                 </div>

                 <div class="modal-body">
                     <p>Are u sure the change the status ?</p>
                 </div>

                 <div class="modal-footer">
                     <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
										  <button type="button" id="btn" onClick="changeStat()" class="btn btn-danger btn-ok">Yes</button>
                 </div>
             </div>
         </div>
     </div>

	<div class="modal fade" id="sendEmail" tabindex="-1" role="dialog">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="exampleModalLabel">Send Email to reporter</h4>
	      </div>
	      <div class="modal-body">
	        <form class="tagForm" id="tag-form" action="../api/fish/siakap.php" method="post">
	          <div class="form-group">
	            <label for="recipient-name" class="control-label">Recipient:</label>
	            <input type="text" class="form-control" id="email" name="email">
	          </div>
	          <div class="form-group">
	            <label for="message-text" class="control-label">Message:</label>
	            <textarea class="form-control" id="cont" name="cont"></textarea>
	          </div>
						</form>

	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        <input id="send" type="submit" class="btn btn-primary" value="Send">
	      </div>

	    </div>
	  </div>
	</div>

<div class="modal fade" id="updateLog" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">Update log for report <?php echo $_GET['id']; ?> </h4>
      </div>
      <div class="modal-body">
        <form class="tagForm" id="tag-form" action="../api/fish/siakap.php" method="post">
          <div class="form-group">
            <label for="message-text" class="control-label">data info:</label>
            <textarea class="form-control" id="info"></textarea>
						<input type="hidden" value="<?php echo $_GET['id'] ?>" id="rid">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <input id="update" type="submit" class="btn btn-primary" value="Update">
      </div>
    </div>
  </div>
</div>

<script src="<?php echo base_url()?>//asset/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url()?>//asset/js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>//asset/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url()?>//asset/js/bootstrap-table.js"></script>
<script src="<?php echo base_url()?>//asset/js/admin.js"></script>
<script src="<?php echo base_url()?>//asset/js/js-cookie.js"></script>
	<script>
	function buka(){
		if(document.getElementById('unlock').checked){
			if(confirm("Unlocking this data may affect the the data , do you wany to continue?")){
				document.getElementById('genInc').disabled = false;
				document.getElementById('btn').disabled = false;
				document.getElementById('delete').disabled = false;
			}
		}

		else {
			document.getElementById('genInc').disabled = true;
			document.getElementById('btn').disabled = true;
			document.getElementById('delete').disabled = true;
			document.getElementById('status').disabled = true;

		}
	}
			!function ($) {
			$(document).on("click","ul.nav li.parent > a > span.icon", function(){
				$(this).find('em:first').toggleClass("glyphicon-minus");
			});
			$(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})
		$(document).ready(function(){
		  var stat  = document.getElementById('start').value;
		  if(stat == "DELIVERED"){
		    document.getElementById('genInc').disabled = true;
				document.getElementById('btn').disabled = true;
				document.getElementById('delete').disabled = true;
				document.getElementById('status').disabled = true;
				document.getElementById('unlock').disabled = false;
		  }else {
				document.getElementById('genInc').disabled = false;
				document.getElementById('btn').disabled = false;
				document.getElementById('delete').disabled = false;
				document.getElementById('unlock').disabled = true;

		  }





		});
	</script>
</body>

</html>
