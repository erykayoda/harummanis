<?php
//require('/var/www/html/ors/dataportal/config/functions.php');
if (isset($this->session->userdata['logged_in'])) {
$username = ($this->session->userdata['logged_in']['username']);
//$email = ($this->session->userdata['logged_in']['email']);
} 
//displayValue();
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>HarumManis - Manage Order</title>

<link href="<?php echo base_url()?>//asset/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url()?>//asset/css/datepicker3.css" rel="stylesheet">
<link href="<?php echo base_url()?>//asset/css/bootstrap-table.css" rel="stylesheet">
<link href="<?php echo base_url()?>//asset/css/styles.css" rel="stylesheet">
<link href="<?php echo base_url()?>//asset/css/font-awesome.min.css" rel="stylesheet">

<!--Icons-->
<script src="<?php echo base_url()?>//asset/js/lumino.glyphs.js"></script>

<!--[if lt IE 9]>
<script src="/asset/js/html5shiv.js"></script>
<script src="/asset/js/respond.min.js"></script>
<![endif]-->

</head>

<body>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"><span></span>HarumManis - BMS</a>
				<ul class="user-menu">
					<li class="dropdown pull-right">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>Welcome	 , <?php echo $username ?> <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> Profile</a></li>
							<li><a href="#"><svg class="glyph stroked gear"><use xlink:href="#stroked-gear"></use></svg> Settings</a></li>
							<li><a href="<?php echo site_url() ?>admin/logout"><svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Logout</a></li>
						</ul>
					</li>
				</ul>
			</div>

		</div><!-- /.container-fluid -->
	</nav>

  <div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form>
		<ul class="nav menu">
			<li><a href="<?php echo site_url() ?>admin/home"><svg class="glyph stroked dashboard-dial"><use xlink:href="#stroked-dashboard-dial"></use></svg> Dashboard</a></li>
			<li><a href="<?php echo site_url() ?>admin/analytic"><svg class="glyph stroked line-graph"><use xlink:href="#stroked-line-graph"></use></svg> Data Analytics</a></li>
			<li class="active"><a href="<?php echo site_url() ?>admin/ManageOrder"><svg class="glyph stroked table"><use xlink:href="#stroked-table"></use></svg> Manage Order</a></li>
			<li><a href="<?php echo site_url() ?>admin/ManagePro"><svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"/></svg> Manage Product</a></li>
			<li><a href="<?php echo site_url() ?>admin/ManageSpp"><svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"/></svg> Manage Supplier</a></li>
			<li><a href="<?php echo site_url() ?>admin/ManageExp"><svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"/></svg> Manage Expense</a></li>
			<li><a href="<?php echo site_url() ?>admin/ManageEmp"><svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"/></svg> Manage Employee</a></li>
		</ul>
	</div><!--/.sidebar-->

	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="index.php"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
				<li class="active">Manage Order</li>
			</ol>
		</div><!--/.row-->

		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header"></h1>
			</div>
		</div><!--/.row-->

		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-12">
						<a class="btn btn-success" href="javascript:void(0)" title="Edit" onclick="admAdd()"><i class="fa fa-plus" aria-hidden="true"></i>  Add Order</a>

				</div>
				</div>
				<hr>
				<div class="panel panel-primary">
					<div class="panel-heading">Order list</div>
					<div class="panel-body">
						<table  data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc" id='data'>
						    <thead>
						    <tr>
						        <th data-field="addr" data-checkbox="true" >Item ID</th>
						        <th data-field="id" data-sortable="true">Order ID</th>
						        <th data-field="date"  data-sortable="true">Date Created</th>
										<th data-field="total"  data-sortable="true">Total Price</th>
										<th data-field="status"  data-sortable="true">Order Status</th>
                    <th data-field="action"  data-sortable="true">Action</th>
						    </tr>
						    </thead>
						</table>
					</div>
				</div>
			</div>
		</div><!--/.row-->
	</div><!--/.main-->


<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Order Details</h3>
            </div>
            <div class="modal-body form">
							<div class="panel panel-primary">
								<div class="panel-heading">Order Details</div>
								<div class="panel-body">
									<form action="#" id="form" class="form-horizontal">
										<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
	                    <input type="hidden" value="" name="id" id="id"/>
	                    <div class="form-body">
	                        <div class="form-group">
	                            <label class="control-label col-md-3">Customer Name:</label>

	                            <div class="col-md-9">
																<b><p id="name" class="form-control">name<p></b>
	                                <span class="help-block"></span>
	                            </div>
	                        </div>
	                        <div class="form-group">
	                            <label class="control-label col-md-3">email:</label>
	                            <div class="col-md-9">
	                              <b><p id="email" class="form-control">email<p></b>
	                                <span class="help-block"></span>
	                            </div>
	                        </div>

													<div class="form-group">
	                            <label class="control-label col-md-3">Contact number:</label>
	                            <div class="col-md-9">
	                              <b><p id="phone" class="form-control">phone num<p></b>
	                                <span class="help-block"></span>
	                            </div>
	                        </div>

													<div class="form-group">
	                            <label class="control-label col-md-3">Postage Address:</label>
	                            <div class="col-md-9">
	                                <textarea name="addr" placeholder="Address" class="form-control" type="text"></textarea>
	                                <span class="help-block"></span>
	                            </div>
	                        </div>
													<div class="form-group">
	                            <label class="control-label col-md-3">Quantity:</label>
	                            <div class="col-md-9">
	                                <b><p id="qty" class="form-control">Quantity<p></b>
	                                <span class="help-block"></span>
	                            </div>
	                        </div>
													<div class="form-group">
	                            <label class="control-label col-md-3">Delivery Method</label>
	                            <div class="col-md-9">
																<b><p id="mthd" class="form-control">Method<p></b>

	                                <span class="help-block"></span>
	                            </div>
	                        </div>
													<div class="form-group">
	                            <label class="control-label col-md-3">Remarks</label>
	                            <div class="col-md-9">
	                                <textarea name="remarks" placeholder="Remarks" class="form-control" type="text"></textarea>
	                                <span class="help-block"></span>
	                            </div>
	                        </div>
													<div class="form-group">
	                            <label class="control-label col-md-3">Status:</label>
	                            <div class="col-md-9">
	                                <select name="stat"  class="form-control">
																		<option value="DELIVERED">DELIVERED</option>
																		<option value="PENDING">TRANSACTION FAILED</option>
																		<option value="NORES">NO PAYMENT</option>
																	</select>
	                                <span class="help-block"></span>
	                            </div>
	                        </div>
	                        <div class="form-group">
	                            <label class="control-label col-md-3">Date order:</label>
	                            <div class="col-md-9">
																<b><p id="date" class="form-control">Method<p></b>

	                                <span class="help-block"></span>
	                            </div>
															<input type="hidden" value="" name="id">
	                        </div>
	                    </div>
	                </form>

								</div>
							</div>
							<div class="panel panel-danger">
								<div class="panel-heading">Item selected</div>
								<div class="panel-body">
									<table data-show-refresh="true"  data-show-refresh="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc" id='bra'>
											<thead>
											<tr>
													<th data-field="id" data-sortable="true">Product ID</th>
													<th data-field="brand" data-sortable="false">Brand</th>
													<th data-field="qty" data-sortable="true">Quantity</th>
													<th data-field="price" data-sortable="true">Each Price</th>
													<th data-field="total" data-sortable="true">Total price</th>
											</tr>
											</thead>
									</table>
								</div>
							</div>

            </div>
            <div class="modal-footer">
                <button type="button" id="genInc" onclick="gen()" class="btn btn-warning">Generate Invoice</button>
								<button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Update</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="new" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Create New Order</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="newIn" class="form-horizontal">
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Customer Name</label>
                            <div class="col-md-9">
                                <input name="name" placeholder="Customer Name" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">email</label>
                            <div class="col-md-9">
                                <input name="email" placeholder="email" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
												<div class="form-group">
                            <label class="control-label col-md-3">Postage Address</label>
                            <div class="col-md-9">
                                <textarea name="addr" placeholder="Address" class="form-control" type="text"></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>
												<div class="form-group">
                            <label class="control-label col-md-3">Quantity</label>
                            <div class="col-xs-3">
                                <input name="qty" placeholder="Quantity" class="form-control" type="number" min="1" max="100" id="ttl">
                                <span class="help-block"></span>
                            </div>
                        </div>

												<div class="form-group">
                            <label class="control-label col-md-3">Upload reciept</label>
                            <div class="col-xs-3">
                                <input type="file" name="resit" id="fileToUpload">
                                <span class="help-block"></span>
                            </div>
                        </div>

												<div id="total"></div>
												<div class="form-group">
                            <label class="control-label col-md-3">Delivery Method</label>
                            <div class="col-md-9">
                                <select name="mthd" class="form-control">
																	<option value="COD" selected="true">Please Select</option>
																	<option value="COD">Cash On Delivery (COD)</option>
																	<option value="POSLAJU">POSLAJU</option>
																</select>
                                <span class="help-block"></span>
                            </div>
                        </div>
												<div class="form-group">
                            <label class="control-label col-md-3">Remarks</label>
                            <div class="col-md-9">
                                <textarea name="remarks" placeholder="Remarks" class="form-control" type="text"></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Date Ordered</label>
                            <div class="col-md-9">
                                <input name="date" placeholder="yyyy-mm-dd" class="form-control datepicker" type="text" disabled="true">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="subOrder()" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- End Bootstrap modal -->
	<script src="<?php echo base_url()?>//asset/js/jquery-1.11.1.min.js"></script>
	<script src="<?php echo base_url()?>//asset/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url()?>//asset/js/bootstrap-datepicker.js"></script>
	<script src="<?php echo base_url()?>//asset/js/bootstrap-table.js"></script>
	<script src="<?php echo base_url()?>//asset/js/admin.js"></script>
	<script src="<?php echo base_url()?>//asset/js/js-cookie.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		go();
	});
	</script>
</body>

</html>
