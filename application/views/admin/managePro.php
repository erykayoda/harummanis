<?php
//require('/var/www/html/ors/dataportal/config/functions.php');
if (isset($this->session->userdata['logged_in'])) {
$username = ($this->session->userdata['logged_in']['username']);
//$email = ($this->session->userdata['logged_in']['email']);
} else {
header("location:admin/login");
}
//displayValue();
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>HarumManis - Manage Order</title>

<link href="<?php echo base_url()?>//asset/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url()?>//asset/css/datepicker3.css" rel="stylesheet">
<link href="<?php echo base_url()?>//asset/css/bootstrap-table.css" rel="stylesheet">
<link href="<?php echo base_url()?>//asset/css/styles.css" rel="stylesheet">
<link href="<?php echo base_url()?>//asset/css/font-awesome.min.css" rel="stylesheet">

<!--Icons-->
<script src="<?php echo base_url()?>//asset/js/lumino.glyphs.js"></script>

<!--[if lt IE 9]>
<script src="/asset/js/html5shiv.js"></script>
<script src="/asset/js/respond.min.js"></script>
<![endif]-->

</head>

<body>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"><span></span>HarumManis - BMS</a>
				<ul class="user-menu">
					<li class="dropdown pull-right">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>Welcome	 , <?php echo $username ?> <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> Profile</a></li>
							<li><a href="#"><svg class="glyph stroked gear"><use xlink:href="#stroked-gear"></use></svg> Settings</a></li>
							<li><a href="<?php echo site_url() ?>admin/logout"><svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Logout</a></li>
						</ul>
					</li>
				</ul>
			</div>

		</div><!-- /.container-fluid -->
	</nav>

  <div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form>
		<ul class="nav menu">
			<li><a href="<?php echo site_url() ?>admin/home"><svg class="glyph stroked dashboard-dial"><use xlink:href="#stroked-dashboard-dial"></use></svg> Dashboard</a></li>
			<li><a href="<?php echo site_url() ?>admin/analytic"><svg class="glyph stroked line-graph"><use xlink:href="#stroked-line-graph"></use></svg> Data Analytics</a></li>
			<li><a href="<?php echo site_url() ?>admin/ManageOrder"><svg class="glyph stroked table"><use xlink:href="#stroked-table"></use></svg> Manage Order</a></li>
			<li class="active"><a href="<?php echo site_url() ?>admin/ManagePro"><svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"/></svg> Manage Product</a></li>
			<li><a href="<?php echo site_url() ?>admin/ManageSpp"><svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"/></svg> Manage Supplier</a></li>
			<li><a href="<?php echo site_url() ?>admin/ManageExp"><svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"/></svg> Manage Expense</a></li>
			<li><a href="<?php echo site_url() ?>admin/ManageEmp"><svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"/></svg> Manage Employee</a></li>
		</ul>
	</div><!--/.sidebar-->

	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="index.php"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
				<li class="active">Manage Product</li>
			</ol>
		</div><!--/.row-->

		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header"></h1>
			</div>
		</div><!--/.row-->



		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-12">
					<a class="btn btn-primary" href="javascript:void(0)" title="Edit" onclick="addPro();"><i class="fa fa-plus" aria-hidden="true"></i>  Add new product</a>


				</div>
				</div>
				<hr>
				<div class="panel panel-primary">
					<div class="panel-heading">Product List</div>
					<div class="panel-body">
						<table  data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc" id='data'>
						    <thead>
						    <tr>
						        <th data-field="id" data-sortable="true" >Product ID</th>
						        <th data-field="name" data-sortable="true">Product Name</th>
										<th data-field="stock" data-sortable="true">Stock status</th>
						        <th data-field="price"  data-sortable="true">Base Price (KG)</th>
										<th data-field="sprice"  data-sortable="true">Selling Price (KG)</th>
                    <th data-field="gpro"  data-sortable="true">Gross Profit (KG)</th>
										<th data-field="issue"  data-sortable="true">Issue By</th>
										<th data-field="action"  data-sortable="true">Action</th>
						    </tr>
						    </thead>

						</table>
					</div>
				</div>
			</div>
		</div><!--/.row-->



	</div><!--/.main-->

<!-- Bootstrap modal -->
<div class="modal fade" id="addProduct" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Order Details</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="formAdd" class="form-horizontal">
					<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
                    <input type="hidden" value="" name="id"/>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Product Name:</label>
                            <div class="col-md-9">
                                <input name="proName" placeholder="Product name" class="form-control" type="text" required="true">
                                <span class="help-block"></span>
                            </div>
                        </div>
												<div class="form-group">
                            <label class="control-label col-md-3">Product type</label>
                            <div class="col-md-9">
                                <select name="type" id="stat" class="form-control">
																	<option value="buah">Buah</option>
																	<option value="jeruk">jeruk</option>
																</select>
                                <span class="help-block"></span>
                            </div>
                        </div>
												<div class="form-group">
                            <label class="control-label col-md-3">Base Price <b>(RM/KG)</b> :</label>
                            <div class="col-md-9">
                                <input name="basePrice" placeholder="Base Price" class="form-control" type="number" required="true">
                                <span class="help-block"></span>
                            </div>
                        </div>
												<div class="form-group">
                            <label class="control-label col-md-3">Sell Price <b>(RM/KG)</b> :</label>
                            <div class="col-md-9">
                                <input name="sellPrice" placeholder="Sell Price" class="form-control" type="number" required="true">
                                <span class="help-block"></span>
                            </div>
                        </div>
												<div class="form-group">
                            <label class="control-label col-md-3">Product Info</label>
                            <div class="col-md-9">
                                <textarea name="info" placeholder="Product info" class="form-control" type="text" required="true"></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>
												<div class="form-group">
                            <label class="control-label col-md-3">Supplier:</label>
                            <div class="col-md-9">
                                <select name="sup" id="stat" class="form-control">
																	<?php foreach($list as $lisst) { ?>
																<option value="<?php echo $lisst['vendorID'] ?>"><?php echo $lisst['companyName'] ?></option>
																<?php } ?>
																</select>
                                <span class="help-block"></span>
                            </div>
                        </div>
												<div class="form-group">
                            <label class="control-label col-md-3">Stock Status</label>
                            <div class="col-md-9">
                                <select name="stat" id="stat" class="form-control">
																	<option value="1">In Stock</option>
																	<option value="0">Out of stock</option>
																</select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="savePro()" class="btn btn-primary">Add product</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<div class="modal fade" id="productinfo" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Order Details</h3>
            </div>
            <div class="modal-body form">
							<form action="#" id="form" class="form-horizontal">
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
									<input type="hidden" value="" name="id"/>
									<div class="form-body">
											<div class="form-group">
													<label class="control-label col-md-3">Product Name:</label>
													<div class="col-md-9">
															<input name="proName" placeholder="Product name" class="form-control" type="text" required="true">
															<span class="help-block"></span>
													</div>
											</div>
											<div class="form-group">
													<label class="control-label col-md-3">Product type</label>
													<div class="col-md-9">
															<select name="type" id="stat" class="form-control">
																<option value="buah">Buah</option>
																<option value="jeruk">jeruk</option>
															</select>
															<span class="help-block"></span>
													</div>
											</div>
											<div class="form-group">
													<label class="control-label col-md-3">Base Price <b>(RM/KG)</b> :</label>
													<div class="col-md-9">
															<input name="basePrice" placeholder="Base Price" class="form-control" type="number" required="true">
															<span class="help-block"></span>
													</div>
											</div>
											<div class="form-group">
													<label class="control-label col-md-3">Sell Price <b>(RM/KG)</b> :</label>
													<div class="col-md-9">
															<input name="sellPrice" placeholder="Sell Price" class="form-control" type="number" required="true">
															<span class="help-block"></span>
													</div>
											</div>
											<div class="form-group">
													<label class="control-label col-md-3">Product Info</label>
													<div class="col-md-9">
															<textarea name="info" placeholder="Product info" class="form-control" type="text" required="true"></textarea>
															<span class="help-block"></span>
													</div>
											</div>
											<div class="form-group">
													<label class="control-label col-md-3">Supplier:</label>
													<div class="col-md-9">
															<select name="sup" id="stat" class="form-control">
																<?php foreach($list as $lisst) { ?>
															<option value="<?php echo $lisst['vendorID'] ?>"><?php echo $lisst['companyName'] ?></option>
															<?php } ?>
															</select>
															<span class="help-block"></span>
													</div>
											</div>
											<div class="form-group">
													<label class="control-label col-md-3">Stock Status</label>
													<div class="col-md-9">
															<select name="stat" id="stat" class="form-control">
																<option value="1">In Stock</option>
																<option value="0">Out of stock</option>
															</select>
															<span class="help-block"></span>
													</div>
											</div>
									</div>
							</form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="updatePro()" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
<script src="<?php echo base_url()?>//asset/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url()?>//asset/js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>//asset/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url()?>//asset/js/bootstrap-table.js"></script>
<script src="<?php echo base_url()?>//asset/js/admin.js"></script>
<script src="<?php echo base_url()?>//asset/js/js-cookie.js"></script>

	<script>
		!function ($) {
			$(document).on("click","ul.nav li.parent > a > span.icon", function(){
				$(this).find('em:first').toggleClass("glyphicon-minus");
			});
			$(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})
		$(document).ready(function(){
			go();
		});
	</script>
</body>

</html>
