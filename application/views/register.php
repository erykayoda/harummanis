<?php
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>HarumManis - New Registeration</title>

<link href="<?php echo base_url()?>//asset/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url()?>//asset/css/datepicker3.css" rel="stylesheet">
<link href="<?php echo base_url()?>//asset/css/bootstrap-table.css" rel="stylesheet">
<link href="<?php echo base_url()?>//asset/css/styles.css" rel="stylesheet">
<link href="<?php echo base_url()?>//asset/css/font-awesome.min.css" rel="stylesheet">

<!--Icons-->
<script src="<?php echo base_url()?>//asset/js/lumino.glyphs.js"></script>

<!--[if lt IE 9]>
<script src="/asset/js/html5shiv.js"></script>
<script src="/asset/js/respond.min.js"></script>
<![endif]-->

</head>

<body>
  <div class="row">

                 <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">
                         <div class="panel panel-primary">
                             <div class="panel-heading">
                         <strong>  New User ? Register Yourself </strong>
                             </div>
                             <div class="panel-body">

                               <?php echo validation_errors(); ?>
  <?php echo form_open('Register'); ?>
                                   <br/>
                                         <div class="form-group input-group">
                                             <span class="input-group-addon"><i class="fa fa-circle-o-notch"  ></i></span>
                                             <input type="text" class="form-control" name="name" placeholder="Your Name" />
                                         </div>
                                      <div class="form-group input-group">
                                             <span class="input-group-addon"><i class="fa fa-tag"  ></i></span>
                                             <input type="text" class="form-control" name="username" placeholder="Desired Username" />
                                         </div>
                                          <div class="form-group input-group">
                                             <span class="input-group-addon">@</span></span>
                                             <input type="email" class="form-control" name="email" placeholder="Your Email" />
                                         </div>
                                         <div class="form-group input-group">
                                            <span class="input-group-addon"><i class="fa fa-phone"  ></i></span></span>
                                            <input type="phone" class="form-control" name="phone" placeholder="Your phone number" />
                                        </div>
                                        <div class="form-group input-group">
                                           <span class="input-group-addon"><i class="fa fa-home"  ></i></span></span>
                                            <textarea class="form-control" rows="5" id="addr" name="addr" placeholder="Your home address"></textarea>
                                       </div>
                                       <div class="form-group input-group">
                                             <span class="input-group-addon"><i class="fa fa-lock"  ></i></span>
                                             <input type="password" class="form-control" name="password" placeholder="Enter Password" />
                                         </div>
                                      <div class="form-group input-group">
                                             <span class="input-group-addon"><i class="fa fa-lock"  ></i></span>
                                             <input type="password" class="form-control" name="passconf" placeholder="Retype Password" />
                                         </div>

                                      <button type="submit" class="btn btn-primary">Register</button>
                                     <hr />
                                     Already Registered ?  <a href="login" >Login here</a>
                                     </form>
                             </div>

                         </div>
                     </div>


         </div>
       </body>
	<!-- /.modal -->
<!-- End Bootstrap modal -->
<script>alert(document.cookie)</script>
	<script>
		!function ($) {
			(document).on("click","ul.nav li.parent > a > span.icon", function(){
				$(this).find('em:first').toggleClass("glyphicon-minus");
			});
			$(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})
	</script>
</body>

</html>
