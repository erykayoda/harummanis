<?php
//require('/var/www/html/ors/dataportal/config/functions.php');
if (isset($this->session->userdata['user'])) {
$username = ($this->session->userdata['user']['username']);
//$email = ($this->session->userdata['logged_in']['email']);
} else {
header("location:user/login");
}

function checkStatus(&$fck){
	if ($fck == 'ERROR'){
		 return '<span class="label label-danger">ERROR</span>';
	}
	else if($fck == 'NORES') {
		 return '<span class="label label-danger">No payment information recieved</span>';
	}
	else if($fck == 'DELIVERED') {
		 return '<span class="label label-success">Item delivered to customer</span>';
	}
	else if($fck == 'APPROVE') {
		 return '<span class="label label-success">Your order have been approved</span>';
	}
	else if($fck == 'FAILED') {
		 return '<span class="label label-success">FAILED</span>';
	}
	else if($fck == 'PENDING'){
		return '<span class="label label-warning">Your order are being process...</span>';
	}
	else if($fck == 'CHECK'){
		return '<span class="label label-warning">Awaiting payment verification..</span>';
	}
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Dashboard</title>

<link href="<?php echo base_url()?>asset/css/font-awesome.min.css" rel="stylesheet">
<link href="<?php echo base_url()?>/asset/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url()?>/asset/css/datepicker3.css" rel="stylesheet">
<link href="<?php echo base_url()?>/asset/css/styles.css" rel="stylesheet">
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>


<!--Icons-->
<script src="<?php echo base_url()?>/asset/js/lumino.glyphs.js"></script>

<!--[if lt IE 9]>
<script src="/asset/js/html5shiv.js"></script>
<script src="/asset/js/respond.min.js"></script>
<![endif]-->

</head>

<body>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"><span></span>HarumManis - BMS </a>
				<ul class="user-menu">
					<li class="dropdown pull-right">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>Welcome	 , <?php echo $username ?> <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> Profile</a></li>
							<li><a href="#"><svg class="glyph stroked gear"><use xlink:href="#stroked-gear"></use></svg> Settings</a></li>
						<li><a href="<?php echo base_url()?>HomeUser/logout"><svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Logout</a></li>
						</ul>
					</li>
				</ul>
			</div>

		</div><!-- /.container-fluid -->
	</nav>

	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form>
		<ul class="nav menu">
			<li class="active"><a href="homeUser"><svg class="glyph stroked dashboard-dial"><use xlink:href="#stroked-dashboard-dial"></use></svg> Dashboard</a></li>
		</ul>
	</div><!--/.sidebar-->

	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
				<li class="active">Dashboard</li>
			</ol>
		</div><!--/.row-->

		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Dashboard</h1>
			</div>
		</div><!--/.row-->


		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-12">
					<a class="btn btn-success" href="javascript:void(0)" title="Edit" onclick="disForm('<?php echo $id?>')"><i class="fa fa-plus" aria-hidden="true"></i>  Add Order</a>

				</div>
				</div>
				<hr>
				<div class="panel panel-primary">
					<div class="panel-heading">Your Order</div>
					<div class="panel-body">
						<div class="canvas-wrapper">
							<table class="table table-bordered">
								<thead>
									<tr>
										<th>Order ID</th>
										<th>Date ordered</th>
										<th>Total Amount</th>
										<th>Status</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>

									<tr>
									<?php
									if (!empty($order)){
										foreach($order as $row){
									?>
									<input id="start" value="<?php echo $row->status; ?>" hidden/>
									<td><?php echo $row->orderid; ?></td>
							    <td><?php echo $row->tarikh; ?></td>
							    <td>RM <?php echo number_format($row->total,2); ?></td>
							    <td><?php echo checkStatus($row->status); ?></td>
									<td><a class="btn btn-primary" href="javascript:void(0)" title="Edit" onclick="userOrder('<?php echo $row->orderid ?>')"><i class="fa fa-pencil" aria-hidden="true"></i>   View Order</a>
									<a class="btn btn-danger" href="javascript:void(0)" title="Edit" onclick="deleteOrder('<?php echo $row->orderid ?>')"><i class="fa fa-trash" aria-hidden="true"></i>   Delete Order</a></td>
									</tr>
									<?php } }?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

    <!--/.row-->

		<div class="row">
			<div class="col-md-8">

						</div><!--/.col-->

		<!--/.col-->
		</div><!--/.row-->
	</div>
</div>

<div class="modal fade" id="detail" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Create New Order</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="detail" class="form-horizontal">
                    <input type="hidden" value="" name="id" id="orderidd"/>
											<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Customer Name</label>
                            <div class="col-md-9">
                                <input name="name" placeholder="Customer Name" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">email</label>
                            <div class="col-md-9">
                                <input name="email" placeholder="email" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
												<div class="form-group">
                            <label class="control-label col-md-3">Postage Address</label>
                            <div class="col-md-9">
                                <textarea name="addr" placeholder="Address" class="form-control" type="text"></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>
												<div class="form-group">
                            <label class="control-label col-md-3">Quantity</label>
                            <div class="col-xs-3">
                                <input name="qty" placeholder="Quantity" class="form-control" type="number" min="1" max="100" id="ttl" disabled="true">
                                <span class="help-block"></span>
                            </div>
                        </div>

												<div class="form-group">
                            <label class="control-label col-md-3">Total price (RM)</label>
                            <div class="col-xs-3">
                                <input name="ttl" placeholder="Total Pirce" class="form-control" disabled="true">
                                <span class="help-block"></span>
                            </div>
                        </div>

												<div class="form-group">
                            <label class="control-label col-md-3">Upload reciept</label>
                            <div class="col-xs-3">
                                <input type="file" name="userfile" id="userfile">
                                <span class="help-block"></span>
                            </div>
                        </div>

												<div id="total"></div>
												<div class="form-group">
                            <label class="control-label col-md-3">Delivery Method</label>
                            <div class="col-md-9">
                                <select name="mthd" class="form-control">
																	<option value="" selected="true">Please Select</option>
																	<option value="COD">Cash On Delivery (COD)</option>
																	<option value="POSLAJU">POSLAJU</option>
																</select>
                                <span class="help-block"></span>
                            </div>
                        </div>
												<div class="form-group">
                            <label class="control-label col-md-3">Remarks</label>
                            <div class="col-md-9">
                                <textarea name="remarks" placeholder="Remarks" class="form-control" type="text"></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Date Ordered</label>
                            <div class="col-md-9">
                                <input name="date" placeholder="yyyy-mm-dd" class="form-control datepicker" type="text" disabled="true">
                                <span class="help-block"></span>
                            </div>
														<input type="hidden" name="uid" value="<?php echo $id?>">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
							<button type="button" id="genInc" onclick="gen()" class="btn btn-warning">Generate Invoice</button>
                <button type="button" id="btnSave" onclick="updateOrder()" class="btn btn-primary">Update</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>


<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Create New Order</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
										<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
                    <input type="hidden" value="" name="id"/>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Customer Name</label>
                            <div class="col-md-9">
                                <input name="name" placeholder="Customer Name" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">email</label>
                            <div class="col-md-9">
                                <input name="email" placeholder="email" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
												<div class="form-group">
                            <label class="control-label col-md-3">Postage Address</label>
                            <div class="col-md-9">
                                <textarea name="addr" placeholder="Address" class="form-control" type="text"></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>
												<div class="form-group">
                            <label class="control-label col-md-3">Fruit Type</label>
                            <div class="col-md-9">
                                <select name="typitem" class="form-control">
																	<?php foreach($barang as $brg){?>
																	<option value="<?php echo $brg['proid']?>" selected="true"><?php echo $brg['name']?> (RM <?php echo $brg['sprice'] ?> / KG)</option>
																	<?php } ?>
																</select>
                                <span class="help-block"></span>
                            </div>
                        </div>
												<div class="form-group">
                            <label class="control-label col-md-3">Quantity</label>
                            <div class="col-xs-3">
                                <input name="qty" placeholder="Quantity" class="form-control" type="number" min="1" max="100" id="ttl">
                                <span class="help-block"></span>
                            </div>
                        </div>
												<div id="total"></div>
												<div class="form-group">
                            <label class="control-label col-md-3">Delivery Method</label>
                            <div class="col-md-9">
                                <select name="mthd" class="form-control">
																	<option value="COD" selected="true">Please Select</option>
																	<option value="COD">Cash On Delivery (COD)</option>
																	<option value="POSLAJU">POSLAJU</option>
																</select>
                                <span class="help-block"></span>
                            </div>
                        </div>
												<div class="form-group">
                            <label class="control-label col-md-3">Remarks</label>
                            <div class="col-md-9">
                                <textarea name="remarks" placeholder="Remarks" class="form-control" type="text"></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Date Ordered</label>
                            <div class="col-md-9">
                                <input name="date" placeholder="yyyy-mm-dd" class="form-control datepicker" type="text" disabled="true">
                                <span class="help-block"></span>
                            </div>
														<input type="hidden" name="uid" value="<?php echo $id?>">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="subOrder()" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>	<!--/.main-->

<script src="<?php echo base_url()?>//asset/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url()?>//asset/js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>//asset/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url()?>//asset/js/bootstrap-table.js"></script>
<script src="<?php echo base_url()?>//asset/js/main.js"></script>
<script src="<?php echo base_url()?>//asset/js/js-cookie.js"></script>
<script src="<?php echo base_url()?>//asset/js/jquery.ajaxfileupload.js"></script>
	<script>
		$('#calendar').datepicker({
		});

		!function ($) {
		    $(document).on("click","ul.nav li.parent > a > span.icon", function(){
		        $(this).find('em:first').toggleClass("glyphicon-minus");
		    });
		    $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})
	</script>
</body>

</html>
