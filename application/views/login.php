<html>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>HarumManis-Login</title>

<link href="<?php echo base_url()?>/asset/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url()?>/asset/css/datepicker3.css" rel="stylesheet">
<link href="<?php echo base_url()?>/asset/css/styles.css" rel="stylesheet">

<!--[if lt IE 9]>
<script src="<?php echo base_url()?>asset/js/html5shiv.js"></script>
<script src="<?php echo base_url()?>asset/js/respond.min.js"></script>
<![endif]-->

</head>

<body>

	<div class="row">
		<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
			<div class="login-panel panel panel-primary">
				<div class="panel-heading">Please Log In (Customer Panel)</div>
				<div class="panel-body">
          <?php echo validation_errors(); ?>
          <?php echo form_open('Validate'); ?>
						<fieldset>
							<div class="form-group">
								<input class="form-control" placeholder="Username" name="username" type="username" required="true">
							</div>
							<div class="form-group">
								<input class="form-control" placeholder="Password" name="password" type="password" required="true">
							</div>
							<div class="checkbox">
								<label>
									<input name="remember" type="checkbox" value="Remember Me">Remember Me
								</label>
							</div>
							<button type="submit" class="btn btn-primary">Log in</button>
							<a href="register" class="btn btn-warning">Register</a>
						</fieldset>
					</form>

				</div>
			</div>
		</div><!-- /.col-->
	</div><!-- /.row -->



	<script src="<?php echo base_url()?>asset/js/jquery-1.11.1.min.js"></script>
	<script src="<?php echo base_url()?>asset/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url()?>asset/js/chart.min.js"></script>
	<script src="<?php echo base_url()?>asset/js/chart-data.js"></script>
	<script src="<?php echo base_url()?>asset/js/easypiechart.js"></script>
	<script src="<?php echo base_url()?>asset/js/easypiechart-data.js"></script>
	<script src="<?php echo base_url()?>asset/js/bootstrap-datepicker.js"></script>
	<script>
		!function ($) {
			$(document).on("click","ul.nav li.parent > a > span.icon", function(){
				$(this).find('em:first').toggleClass("glyphicon-minus");
			});
			$(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})
	</script>
</body>

</html>
