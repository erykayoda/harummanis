<?php
Class Anal extends CI_Model
{
 function getData()
 {
   //$this->load->database();
   $query = $this -> db -> query('select * from orders');
   return $query->result();
 }

 function sales()
 {
   //$this->load->database();
   $query = $this -> db -> query('select sum(total) as total from orders where status = "DELIVERED"');
   return $query->row()->total;
 }

 function modal()
 {
   $query = $this -> db -> query('select sum(sprice) as total from product');
   return $query->row()->total;
 }

 function expense()
 {
   //$this->load->database();
   $query = $this -> db -> query('select sum(total) as total from expenses');
   return $query->row()->total;
 }

 function total()
 {
   //$this->load->database();
   $query = $this -> db -> query('select * from orders');
   return $query->num_rows();
 }

//update order record
public function update($data)
{
  /*
  extract($data);
  $this->db->where('orders', $id);
  $this->db->update($table_name, array('title' => $title));
  return true;*/

  $this->db->set('status',$data['stat']);
  $this->db->where('orderid',$data['id']);
  $this->db->update('orders');
  return true;

}


 function bulansales()
 {
   //$this->load->database();
   $query = $this -> db -> query('select  COUNT(*) as total , monthname(date) as bulan from orders group by bulan DESC');
   return $query->result();
 }

 function masuk()
 {
   //$this->load->database();
   $query = $this -> db -> query('select masuk , COUNT(*) as total from orders group by masuk');
   return $query->result();
 }

 function pending()
 {
   //$this->load->database();
   $query = $this -> db -> query('select * from orders where status = "PENDING"');
   return $query->num_rows();
 }

 function deliver()
 {
   //$this->load->database();
   $query = $this -> db -> query('select * from orders where status = "DELIVERED"');
   return $query->num_rows();
 }
 public function save($data)
{
  $this->db->insert('orders', $data);
  return $this->db->insert_id();
}

public function test($id)
{
  $this->db->select('*');
  $this->db->from('orders');
  $this->db->where('orderid',$id);
  $query = $this->db->get();
  return $query->row();
}

public function get_by_id($id)
{
  $this->db->select('*');
  $this->db->from('orders');
  $this->db->where('userid',$id);
  $query = $this->db->get();
  return $query->result();
}
}

?>
