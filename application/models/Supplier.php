<?php
Class Supplier extends CI_Model
{
 function getData()
 {
   $query = $this -> db -> query('select * from vendor');
   return $query->result();
 }
 function expense()
 {
   $query = $this -> db -> query('select sum(assetPrice) as total from asset');
   return $query->row()->total;
 }

 public function updateCom($data){
   $this->db->set('companyName',$data['companyName']);
   $this->db->set('email',$data['email']);
   $this->db->set('info',$data['info']);
   $this->db->set('address',$data['address']);
   $this->db->set('tel',$data['tel']);
   $this->db->set('pic',$data['pic']);
   $this->db->set('ssmNo',$data['ssmNo']);
   $this->db->where('vendorID',$data['id']);
   $this->db->update('vendor');
   return true;
 }

 public function saveSup($data)
 {
   $this->db->insert('vendor', $data);
   return true;
 }

 public function save($data)
   {
       $this->db->insert('orders', $data);
       return $this->db->insert_id();
   }

   public function deleteComInfo($id)
   {
     $this->db->where('vendorID',$id);
     $this->db->delete('vendor');
     return true;
   }


   public function test($id)
     {
         $this->db->select('*');
         $this->db->from('orders');
         $this->db->where('orderid',$id);
         $query = $this->db->get();

         return $query->row();
     }

 public function get_by_id($id)
   {
       $this->db->select('*');
       $this->db->from('vendor');
       $this->db->where('vendorID',$id);
       $query = $this->db->get();
       return $query->row();
   }


}
?>
