<?php
Class Product extends CI_Model
{
 function getData()
 {
   //$this->load->database();
   $this->db->select('*');
   $this->db->from('product');
   $this->db->join('vendor','vendor.vendorID = product.vendorID');
   $query = $this->db->get();
   return $query->result();
 }

 public function comList(){
   $query = $this -> db -> query('select * from vendor');
   return $query->result_array();
 }
 public function apeCom($id){

   $this->db->select('companyName');
   $this->db->from('vendor');
   $this->db->where('vendorID',$id);
   $query = $this->db->get();
   return $query->row();
 }

  public function savePro($data)
  {
    $this->db->insert('product', $data);
    return true;
  }

  public function updatePro($data){
    $this->db->set('name',$data['name']);
    $this->db->set('vendorID',$data['vendorID']);
    $this->db->set('stat',$data['stat']);
    $this->db->set('price',$data['price']);
    $this->db->set('sprice',$data['sprice']);
    $this->db->set('gprofit',$data['gprice']);
    $this->db->set('proInfo',$data['info']);
    $this->db->set('type',$data['type']);
    $this->db->where('proid',$data['proid']);
    $this->db->update('product');
    return true;
  }

  public function deleteInfo($id)
  {
    $this->db->where('proid',$id);
    $this->db->delete('product');

    $done = 'DONE!';
    return $done;
  }

  public function getInfo($id)
  {
    $this->db->select('*');
    $this->db->from('product');
    $this->db->join('vendor','vendor.vendorID = product.vendorID');
    $this->db->where('product.proid',$id);
    $query = $this->db->get();
    return $query->row();

  }

}
?>
