<?php
Class User extends CI_Model
{
 function login($username, $password)
 {
   $this -> db -> select('userid, uname, passwd , level');
   $this -> db -> from('users');
   $this -> db -> where('uname', $username);
   $this -> db -> where('passwd',$password);
   $this -> db -> limit(1);

   $query = $this -> db -> get();

   if($query -> num_rows() == 1)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
 }

 public function addUsr($data)
 {
   $this->db->insert('users', $data);
   return true;
 }

 public function push_resit($data)
 {
   $this->db->insert('resit_bank', $data);
   return TRUE;
 }

 public function test($id)
 {
   $this->db->select('*');
   $this->db->from('orders');
   $this->db->join('users','orders.userid = users.userid');
   $this->db->join('order_item','orders.orderid = order_item.orderid');
   $this->db->where('orders.orderid',$id);
   $query = $this->db->get();
   return $query->row();
 }

 public function getUser($id)
   {
       $this->db->select('*');
       $this->db->from('users');
       $this->db->where('userid',$id);
       $query = $this->db->get();

       return $query->row();
   }

   public function getInfo($id)
   {
     $this->db->select('*');
     $this->db->from('product');
     $this->db->join('vendor','vendor.vendorID = product.vendorID');
     $this->db->where('product.proid',$id);
     $query = $this->db->get();
     return $query->result_array();

   }

   public function listBrg(){
     $query = $this -> db -> query('select * from product where stat  = 0');
     return $query->result_array();
   }

   function chk($in){
     $this -> db -> select('uname');
     $this -> db -> from('users');
     $this -> db -> where('uname', $in);
     $this -> db -> limit(1);
     $query = $this -> db -> get();
     if($query -> num_rows() == 1)
     {
       return FALSE;
     }
     else
     {
       return TRUE;
     }
   }

   public function updateStat($data){
     $this->db->set('status',$data['status']);
     $this->db->where('orderid',$data['orderid']);
     $this->db->update('orders');
     return true;
   }

   public function updateOder($data){
     $this->db->set('name',$data['name']);
     $this->db->set('vendorID',$data['vendorID']);
     $this->db->set('stat',$data['stat']);
     $this->db->set('price',$data['price']);
     $this->db->set('sprice',$data['sprice']);
     $this->db->set('gprofit',$data['gprice']);
     $this->db->set('proInfo',$data['info']);
     $this->db->set('type',$data['type']);
     $this->db->where('proid',$data['proid']);
     $this->db->update('product');
     return true;
   }

   public function deleteOdor($id)
   {
     $this->db->where('orderid',$id);
     $this->db->delete('orders');
     return true;
   }

   public function allorder($id)
   {

     $this->db->select('orderid');
     $this->db->select('date(date) as tarikh');
     $this->db->select('total');
     $this->db->select('status');
     $this->db->from('orders');
     $this->db->where('userid',$id);
     $query = $this->db->get();
     return $query->result();
   }

 function login_user($username, $password)
 {
   $this -> db -> select('userid, uname, passwd');
   $this -> db -> from('users');
   $this -> db -> where('uname', $username);
   $this -> db -> where('passwd',$password);
   $this -> db -> limit(1);
   $query = $this -> db -> get();

   if($query -> num_rows() == 1)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
 }
}
?>
