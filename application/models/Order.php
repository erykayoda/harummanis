<?php
Class Order extends CI_Model
{
 function getData()
 {
   //$this->load->database();
   $this->db->select('*');
   $this->db->from('orders');
   $this->db->join('users','orders.userid = users.userid');
   $this->db->order_by("date", "desc");
   $query = $this->db->get();
   return $query->result();
 }

//update order record
public function update($data)
{
  $this->db->set('status',$data['stat']);
  $this->db->where('orderid',$data['id']);
  $this->db->update('orders');
  return true;

}

 public function save($data)
{
  $this->db->insert('orders', $data);
  return $this->db->insert_id();
}

public function listorder($data)
{
  $this->db->insert('order_item', $data);
  return $this->db->insert_id();
}

public function deleteInfo($id)
{
  $this->db->where('orderid',$id);
  $this->db->delete('orders');
  return true;
}

public function printInv($id)
{
  $this->db->select('name,price,qty,sprice,product.proid');
  $this->db->from('product');
  $this->db->join('order_item','product.proid = order_item.proid');
  $this->db->where('order_item.orderid',$id);
  $query = $this->db->get();
  //var_dump($query);
  return $query->result_array();

}

public function orderInf($id)
{
  $this->db->select('name,price,qty,sprice,product.proid');
  $this->db->from('product');
  $this->db->join('order_item','product.proid = order_item.proid');
  $this->db->where('order_item.orderid',$id);
  $query = $this->db->get();
  //var_dump($query);
  return $query->result();

}

public function resit_img($id)
{
  $this->db->select('*');
  $this->db->from('resit_bank');
  //$this->db->join('resit_bank','resit_bank.orderid = orders.orderid');
  $this->db->where('orderid',$id);
  $query = $this->db->get();
  return $query->row_array();
}
public function vieworr($id)
{
  $this->db->select('uname,date,del_mthd,addr,email,orders.orderid,status,orders.userid,masuk');
  //$this->db->select('*');
  $this->db->from('orders');
  $this->db->join('users','orders.userid = users.userid');
  //$this->db->join('resit_bank','resit_bank.orderid = orders.orderid');
  $this->db->where('orders.orderid',$id);
  $query = $this->db->get();
  return $query->row_array();
}



public function get_by_id($id)
{
  $this->db->select('*');
  $this->db->from('orders');
  $this->db->where('userid',$id);
  $query = $this->db->get();
  return $query->result();
}
}

?>
