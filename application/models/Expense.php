<?php
Class Expense extends CI_Model
{
 function getData()
 {
   //$this->load->database();
   $this->db->select('*');
   $this->db->from('expenses');
   $this->db->join('exp_type','exp_type.typID = expenses.typID');
   $this->db->join('users','users.userid = expenses.userid');
   $query = $this->db->get();
   return $query->result();
 }

 function typeAdd($data){
   $this->db->insert('exp_type', $data);
   return true;
 }

 public function typeList(){
   $query = $this -> db -> query('select * from exp_type');
   return $query->result_array();
 }

 public function type(){
   $this->db->select('*');
   $this->db->from('exp_type');
   $this->db->join('users','users.userid = exp_type.userid');
   $query = $this->db->get();
   return $query->result();
 }

 public function comList(){
   $query = $this -> db -> query('select * from vendor');
   return $query->result_array();
 }

 public function apeCom($id){

   $this->db->select('companyName');
   $this->db->from('vendor');
   $this->db->where('vendorID',$id);
   $query = $this->db->get();
   return $query->row();
 }

  public function saveExp($data)
  {
    $this->db->insert('expenses', $data);
    return true;
  }

  public function updateExp($data){
    $this->db->set('name',$data['name']);
    $this->db->set('vendorID',$data['vendorID']);
    $this->db->set('stat',$data['stat']);
    $this->db->set('remarks',$data['remarks']);
    $this->db->set('typID',$data['typID']);
    $this->db->set('total',$data['total']);
    $this->db->where('expid',$data['expid']);
    $this->db->update('expenses');
    return true;
  }

  public function deleteInfo($id)
  {
    $this->db->where('expid',$id);
    $this->db->delete('expenses');
    return true;
  }

  public function deleteTyp($id)
  {
    $this->db->where('typID',$id);
    $this->db->delete('exp_type');
    return true;
  }

  public function chk($id)
  {
    $this->db->select('*');
    $this->db->from('expenses');
    $this->db->where('typID',$id);
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function getInfo($id)
  {
    $this->db->select('*');
    $this->db->from('expenses');
    $this->db->join('exp_type','exp_type.typID = expenses.typID');
    $this->db->where('expenses.expid',$id);
    $query = $this->db->get();
    return $query->row();

  }

}
?>
