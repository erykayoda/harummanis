
$(document).ready(function() {
    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("textarea").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("select").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });

});

//console.log(test['status']);
/*
var statVal = document.getElementById('start');
var t = statVal.value;
console.log(t);
if (t == "CHECK"){
	document.getElementById("btn").disabled=true;
  document.getElementById("upt").disabled=true;
}else {
	document.getElementById("btn").disabled=false;
  document.getElementById("upt").disabled=false;
}
*/

 function admAdd(){
   $('#newIn')[0].reset();
   $('.form-group').removeClass('has-error');
   $('.help-block').empty();
   $('[name="date"]').datepicker('setDate',new Date());
   $('#new').modal('show');
 }

function disForm(id){
  $('#form')[0].reset();
  $('.modal-title').text('Create new order');
  $('[name="date"]').datepicker('setDate',new Date());

  $.ajax({
    url: window.location.href + '/userDetail/' +id,
    type:"GET",
    dataType:"JSON",
    success: function(data){
      $('[name="id"]').val(data.orderid);
      $('[name="name"]').val(data.uname);
      $('[name="email"]').val(data.email);
      $('[name="addr"]').val(data.addr);
      $('#modal_form').modal('show');
    }
  });
}

function updateOrder()
{
    if(confirm("Are you sure that the entered details are correct?"))
    {
      var token_id = Cookies.get('harum_cookie');
      var id = document.getElementById("orderidd").value;
      var token = {
        secure_token : token_id,
        orderid:id
      }

      console.log(token);
      $.ajaxFileUpload({
              url:window.location.href + '/updateOrder',
        secureuri:false,
    fileElementId:'userfile',
             data:token,
         dataType: "JSON",
         success : function (data)
         {
           var info = jQuery.parseJSON(data)
           console.log(info.msg);
           alert(info.msg);
           location.reload();
         }
       });
       return false;
     }
   }

function subOrder()
{

    if(confirm("Are you sure that the entered details are correct?"))
    {

      $.ajax({
          url : window.location.href + "/addorder/",
          type: "POST",
          data: $('#form').serialize(),
          dataType: "JSON",
          success: function(data)
          {
            console.log(data);
            if(data.status == "ERR"){
              alert(data.msg);
            }
            else {
              alert("Your order has been created!");
              $('#modal_form').modal('hide');
              location.reload();
            }
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              alert('Error adding / update data');

          }
      });
    }

}

function deleteOrder(id)
{

  if(confirm("Are you sure want to delet the order ?"))
  {
    var token_id = Cookies.get('harum_cookie');
    var token = {
      secure_token : token_id,
    }
      $.ajax({
          url : window.location.href + "/delete/" + id,
          type: "POST",
          data:token,
          dataType: "JSON",
          success: function(data)
          {
            if(data){
              alert("Sucessfully deletes");
              location.reload();
            }
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              alert('ERROR');
          }
      });
  }

}

function userOrder(id)
{
    save_method = 'update';
    $('#form')[0].reset();
    $('.form-group').removeClass('has-error');
    $('.help-block').empty();


    $.ajax({
        url : window.location.href + "/view/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
          console.log(data);
            $('[name="id"]').val(data.orderid);
            $('[name="name"]').val(data.uname);
            $('[name="email"]').val(data.email);
            $('[name="addr"]').val(data.addr);
            $('[name="qty"]').val(data.qty);
            $('[name="qty"]').val(data.qty);
            $('[name="ttl"]').val(data.total);
            $('[name="remarks"]').val(data.remarks);
            $('[name="stat"]').val(data.status);
            if(data.status == "CHECK"){
              document.getElementById('btnSave').disabled = true;
              document.getElementById('genInc').disabled = true;
              document.getElementById('userfile').disabled = true;
            }else if(data.status == "APPROVE") {
              document.getElementById('btnSave').disabled = true;
              document.getElementById('genInc').disabled = false;
            }else if(data.status == "DELIVERED"){
              document.getElementById('userfile').disabled = true;
              document.getElementById('genInc').disabled = false;
            }else if(data.status == "NORES"){
              document.getElementById('userfile').disabled = false;
              document.getElementById('genInc').disabled = true;
            }
            else {
              document.getElementById('btnSave').disabled = true;
              document.getElementById('genInc').disabled = true;
              document.getElementById('userfile').disabled = true;
            }
            $('[name="date"]').val(data.date);
            $('#detail').modal('show');
            $('.modal-title').text('Order records for '+ data.orderid) ;
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('ERROR');
        }
    });
}
