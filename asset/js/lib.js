function goto(a,b){
  console.log(a,b);
  //bounds = new L.LatLngBounds(a,b);
  map.fitBounds([[a,b]]);
}


function displayInfo(){

}

var sidebar = $('#sidebar').sidebar();
var map = new L.Map('map', {center: new L.LatLng(2.977367, 101.731059), zoom: 9 , zoomControl: false});
var gmap = new L.Google('HYBRID');
map.addLayer(gmap);
new L.Control.Zoom({position: 'bottomright'}).addTo(map);


function main(){
//uniten polygon data (for PIP)
$.ajax({
  type:'POST',
  url:"data/uniten.geojson",
  dataType:'json'
}).done(function(response){
  unitenLayer = L.geoJson(response,{
  }).bindPopup('Universiti Tenega Nasional')
  .addTo(map);
  //console.log(response);
});

//mffa dummy data
$.ajax({
  type:'POST',
  url:"data/mffa.geojson",
  dataType:'json'
}).done(function(response){
  var truck = L.icon({
        iconUrl: 'js/images/truck.png'
    });
  mffa = L.geoJson(response,{
          pointToLayer: function (feature, latlng) {
              return L.marker(latlng, {icon: truck })}
          }).bindPopup('MFFA Team').addTo(map);
});

//mobile-app source (live API)
$.ajax({
  type:'POST',
  url:'http://erykayoda/ors/api/index.php',
  dataType:'json'
}).done(function(response){
  mainLayer = L.geoJson(response ,{
      onEachFeature: function(feature,layer){
      var info = [];
      for (var isi in feature.properties){
        info.push(isi + ":" + feature.properties[isi]);
      }
      layer.bindPopup(info.join("</br>"));
    }
  }).addTo(map);



  var arrayLength = response['features'].length;

  function goto(a,b){
    console.log(a,b);
  }
  //console.log(arrayLength);

  //display Notifications Data
  var  listReport = '<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">';
  listReport += '<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>';
  listReport += '<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>';
  listReport += '<table class="table table-boarded"><tr><td><b>No.</b></td>';
  listReport += "<td><b>Timestamp</b></td>";
  listReport +="<td><b>Description</b></td>";
  listReport +="<td><b>Action</b></td></tr>";

  for (var i = 0; i < arrayLength; i++) {
    //console.log(response['features'][i]['geometry']['coordinates']['0']);
    listReport +="<tr><td>"+ i +"</td>";
    listReport +="<td>"+ response['features'][i]['properties']['Timestamp'] +"</td>";
    listReport +="<td>"+ response['features'][i]['properties']['Description'] +"</td>";
    listReport +='<td><button type="button" class="btn btn-primary" id="test" onClick="goto('+response['features'][i]['geometry']['coordinates']['1'] +','+ response['features'][i]['geometry']['coordinates']['0']+')">Check</button></td></tr>';

  }

  listReport +="</table>";
  document.getElementById("listReport").innerHTML = listReport;


});
}
