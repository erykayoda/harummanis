
var end = ["bG9hZERhdGE=" , "ZGVsZXRl" ,"ZGV0YWls" , "AAAAaHR0cDovL2VyeWtheW9kYS9oYXJ1bS9hZG1pbi9NYW5hZ2VPcmRlci8=" , "Z2VucGRm"]

function go(){$.ajax({url:window.location.href+"/" + atob(end[0]) + "/",type:"GET",dataType:"JSON",success:function(a){$("#data").bootstrapTable({data:a})}})};

$('.datepicker').datepicker({
    autoclose: true,
    format: "yyyy-mm-dd",
    todayHighlight: true,
    orientation: "top auto",
    todayBtn: true,
    todayHighlight: true,
});

function admAdd(){
  $('#newIn')[0].reset();
  $('.form-group').removeClass('has-error');
  $('.help-block').empty();
  $('[name="date"]').datepicker('setDate',new Date());
  $('#new').modal('show');
}

function disForm(id){
  $('#form')[0].reset();
  $('.form-group').removeClass('has-error');
  $('.help-block').empty();
  $('[name="date"]').datepicker('setDate',new Date());
}

function addCom()
{
    save_method = 'add';
    $('#formCom')[0].reset();
    $('.form-group').removeClass('has-error');
    $('.help-block').empty();
    $('#addCompany').modal('show');
    $('.modal-title').text('Add new product');
}


function expType(){
  save_method = 'add';
  $('#addExpType')[0].reset();
  $('.form-group').removeClass('has-error');
  $('.help-block').empty();
  $('#addtype').modal('show');
  $('.modal-title').text('Add new expense');
}
function addExp(){
  save_method = 'add';
  $('#addExpd')[0].reset();
  $('.form-group').removeClass('has-error');
  $('.help-block').empty();
  $('#modal_form').modal('show');
  $('.modal-title').text('Add new expense');
}


function saveCom()
{
    $('#btnSave').text('saving...');
    $('#btnSave').attr('disabled',true);
    var url;


    $.ajax({
        url : window.location.href + '/add',
        type: "POST",
        data: $('#formCom').serialize(),
        dataType: "JSON",
        success: function(data)
        {
          console.log(data);

            if(data.status == "ERR")
            {
              alert(data.msg);
            }
            else {
              $('#addCompany').modal('hide');
              $('#formCom')[0].reset();
              location.reload();
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('ERROR');
        }
    });
}


function deleteCom(id){
  if(confirm("Are you sure ?"))
  {
    var token_id = Cookies.get('harum_cookie');
    var token = {
      secure_token : token_id,
    }
    $.ajax({
      url: window.location.href + "/deleteCom/" + id,
      type:'POST',
      data:token,
      success:function(data)
      {
        alert("details deleted");
        $('#productinfo').modal('hide');
        location.reload();
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error deleting data');
      }
    })
  }
}


function updateCom()
{
    $.ajax({
        url : window.location.href + '/update',
        type: "POST",
        data: $('#displayCom').serialize(),
        dataType: "JSON",
        success: function(data)
        {
          if(data.status == "ERR")
          {
            alert(data.msg);
          }
          else {
            $('#addCompany').modal('hide');
            $('#formCom')[0].reset();
            location.reload();
          }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('ERROR');
        }
    });
}

function displayCom(id)
{
  $('#displayCom')[0].reset();
  $.ajax({
      url : window.location.href + "/" + atob(end[2]) + "/" + id,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {
          $('[name="id"]').val(data.vendorID);
          $('[name="name"]').val(data.companyName);
          $('[name="email"]').val(data.email);
          $('[name="addr"]').val(data.address);
          $('[name="stat"]').val(data.stat);
          $('[name="tel"]').val(data.tel);
          $('[name="info"]').val(data.info);
          $('[name="ssm"]').val(data.ssmNo);
          $('[name="pic"]').val(data.pic);
          $('[name="date"]').val(data.created);
          $('#disCom').modal('show');
          $('.modal-title').text('Supplier info for  '+ data.companyName) ;
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('ERROR');
      }
  });
}

//expense
function deleteExp(id){

  if(confirm("Are you sure ?"))
  {
    var token_id = Cookies.get('harum_cookie');
    var token = {
      secure_token : token_id,
    }

    $.ajax({
      url: window.location.href + "/" + atob(end[1]) + "/" + id,
      type:'POST',
      data:token,
      success:function(data)
      {

        alert("details deleted");
        console.log(data);
        $('#productinfo').modal('hide');
        location.reload();
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('ERROR');
      }
    })
  }
}



function viewExp(id)
{
  $('#expview')[0].reset();
  $.ajax({
      url : window.location.href  + "/" + atob(end[2]) + "/"  + id,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {
          console.log(data.typID);
          if(data.stat == 1){
            $('#btnUpd').attr('disabled',true);
          }
          else {
            $('#btnUpd').attr('disabled',false);
          }
          $('[name="id"]').val(data.expid);
          $('[name="expname"]').val(data.name);
          $('[name="ven"]').val(data.vendorID);
          $('[name="typ"]').val(data.typID);
          $('[name="stat"]').val(data.stat);
          $('[name="total"]').val(data.total);
          $('[name="info"]').val(data.remarks);
          $('#exp_view').modal('show');
          $('.modal-title').text('Expense info for  '+ data.expid) ;

      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('ERROR');
      }
  });
}

function updateExp(){

  $.ajax({
      url : window.location.href + '/updateExpense/',
      type: "POST",
      data: $('#expview').serialize(),
      dataType: "JSON",
      success: function(data)
      {
          console.log(data);
          if(data)
          {
              $('#exp_view').modal('hide');
              $('#addExpd')[0].reset();
              location.reload();
          }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('ERROR');
      }
  });
}


function addExpense(){

  $.ajax({
      url : window.location.href + '/add',
      type: "POST",
      data: $('#addExpd').serialize(),
      dataType: "JSON",
      success: function(data)
      {
          console.log(data);
            if(data.status == "ERR"){
              alert(data.msg);
            }
            else {
              $('#modal_form').modal('hide');
              $('#addExpd')[0].reset();
              location.reload();
            }

      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          console.log(jqXHR);
          alert('ERROR');
      }
  });
}

function deleteTyp(id){

  if(confirm("Are you sure ?"))
  {
    var token_id = Cookies.get('harum_cookie');
    var token = {
      secure_token : token_id,
    }

    $.ajax({
      url: window.location.href + "/deleteType/"+ id,
      type:'POST',
      data:token,
      dataType: "JSON",
      success:function(data)
      {
        if(data.status == "ERR"){
          alert(data.msg);
        }
        else {
          alert("details deleted");
          location.reload();
        }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('ERROR');
      }
    })
  }
}

function saveType(){

  $.ajax({
      url : window.location.href + '/addType',
      type: "POST",
      data: $('#addExpType').serialize(),
      dataType: "JSON",
      success: function(data)
      {
          console.log(data);
          if(data)
          {
              $('#addtype').modal('hide');
              $('#addExpType')[0].reset();
              //reload_table();
          }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('ERROR');

      }
  });
}



//product
function addPro()
{
    $('#formAdd')[0].reset();
    $('#addProduct').modal('show');
    $('.modal-title').text('Add new product');
}

function updatePro()
{
    $('#btnSave').text('saving...');
    $('#btnSave').attr('disabled',true);
    var url;


    $.ajax({
        url : window.location.href + '/updatePro',
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {
            console.log(data);
            if(data.status)
            {
                $('#productinfo').modal('hide');
                location.reload();
            }

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('ERROR');

        }
    });
}

function viewpro(id)
{

    $('#form')[0].reset();

    $.ajax({
        url : window.location.href + "/" + atob(end[2]) + "/"  + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            console.log(data);
            $('[name="id"]').val(data.proid);
            $('[name="basePrice"]').val(data.price);
            $('[name="sellPrice"]').val(data.sprice);
            $('[name="proName"]').val(data.name);
            $('[name="info"]').val(data.proInfo);
            $('[name="type"]').val(data.type);
            $('[name="mthd"]').val(data.del_mthd);
            $('[name="sup"]').val(data.vendorID);
            $('[name="remarks"]').val(data.remarks);
            $('[name="stat"]').val(data.stat);
            $('[name="date"]').val(data.date);
            $('#productinfo').modal('show');
            $('.modal-title').text('Product info for '+ data.name) ;

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('ERROR');
        }
    });
}

function deletePro(id){

  if(confirm("Are you sure ?"))
  {
    var token_id = Cookies.get('harum_cookie');
    var token = {
      secure_token : token_id,
    }
    $.ajax({
      url: window.location.href + "/deletePro/" + id,
      type:'POST',
      data:token,
      success:function(data)
      {
        alert("data DELETED");
        $('#productinfo').modal('hide');
        location.reload();
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error deleting data');
      }
    })
  }
}


function savePro()
{
  $.ajax({

      url : window.location.href + '/add',
      type: "POST",
      data: $('#formAdd').serialize(),
      dataType: "JSON",
      success: function(data)
      {
          console.log($('#formAdd').serialize());
          if(data.status)
          {
              $('#addProduct').modal('hide');
              $('#formAdd')[0].reset();
              location.reload();
          }
          $('#btnSave').text('save');
          $('#btnSave').attr('disabled',false);


      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error Ajax');
      }
    });
}


function deleteOdr(id){

  if(confirm("Are you sure ?"))
  {
    var token_id = Cookies.get('harum_cookie');
    var id = document.getElementById("id").value;
    var token = {
      secure_token : token_id,
      id:id,
    }
    //console.log(window.location.origin);

    $.ajax({
      url: window.location.origin + "/harum/admin/manageOrder/deleteOrder/",
      type:'POST',
      data : token,
      success:function(data)
      {
        alert("details deleted");
        //console.log(data);
        location.reload();
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error deleting data');
      }
    })
  }
}


function viewOdr(id)
{
    save_method = 'update';
    $('#form')[0].reset();
    $('#bra').bootstrapTable('destroy');
    $('.form-group').removeClass('has-error');
    $('.help-block').empty();

    $.ajax({
        url : window.location.href + "/view/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
          $.ajax({
            url : window.location.href + "/ordData/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(test)
            {
              $('#bra').bootstrapTable({
              data:test,
              });
            }
          })
            console.log(data[0]);
            for(i  = 0 ; i < data.length;i++)
            {
              document.getElementById("name").innerHTML = data[0].name;
              document.getElementById("email").innerHTML = data[0].email;
              document.getElementById("qty").innerHTML = data[0].qty;
              document.getElementById("date").innerHTML = data[0].date;
              document.getElementById("mthd").innerHTML = data[0].del;
              document.getElementById("phone").innerHTML = data[0].phone;
              //document.getElementById("stat").innerHTML = data[0].del;
              //document.getElementById("p1").innerHTML
              $('[name="id"]').val(data[0].id);
              $('[name="userid"]').val(data[0].userid);
              $('[name="name"]').val(data[0].name);
              $('[name="email"]').val(data[0].email);
              $('[name="addr"]').val(data[0].addr);
              $('[name="qty"]').val(data[0].qty);
              $('[name="mthd"]').val(data[0].del_mthd);
              $('[name="remarks"]').val(data[0].remarks);
              $('[name="stat"]').val(data[0].status);
              $('[name="date"]').val(data.date);
              $('.modal-title').text('Order records for '+ data[0].id) ;
            }
            $('#modal_form').modal('show');
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('ERROR');
        }
    });
}


function changeStat(){
  if(confirm("Are you sure to change the status ?")){
    var sel  = document.getElementById('status').value;
  	var id = document.getElementById('id').value;
    var token_id = Cookies.get('harum_cookie');
    var token = {
      secure_token:token_id,
      stat:sel,
      id:id
    }
    console.log(token);
  	$.ajax({
  		url: atob(end[3]) + "/updateOrder/",
      type:'POST',
      data:token,
  	}).then(function(response){
      alert("SUCCESS");
      location.reload(true);
      return false;

  		//$('#btn').hide();
  	})
  }
}



function gen()
{
  var stat  = document.getElementById('start').value;
  if(confirm("Print receipt ?")){
    //document.getElementById('genInc').disabled = true;
    location.reload();
    var id = document.getElementById("id").value;
    var token_id = Cookies.get('harum_cookie');
    var token = {
      secure_token:token_id,
      id:id
    }
    $.ajax({
  		url: atob(end[3]) + atob(end[4]) +  "/",
      type:'POST',
      data:token,
      dataType: "text",
      responseType:'arraybuffer',
      //processData: false,
  	}).then(function(response){
      var blob = new Blob([response], {type: "application/pdf"});
      var objectUrl = URL.createObjectURL(blob);
      window.open(objectUrl);
  	})
  }

}

function save()
{
    $.ajax({
        url : window.location.href + '/updateOrder/',
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {
            console.log(data);
            if(data.status)
            {
              $('#modal_form').modal('hide');
            }
            $('#btnSave').text('save');
            $('#btnSave').attr('disabled',false);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('ERROR');
            $('#btnSave').text('save');
            $('#btnSave').attr('disabled',false);

        }
    });
}
