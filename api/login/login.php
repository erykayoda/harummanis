<?php
require("../db/db.php");
header("Content-Type: application/json; charset=UTF-8");
if (isset($_SERVER['HTTP_ORIGIN'])){
  header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
  header("Access-Control-Allow-Credentials: true");
  header("Access-Control-Max-Age: 86400");
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

  if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
  header("Access-Control-Allow-Methods:POST");

  if(isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
  header("Access-Control-Allow-Headers:{$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

  exit(0);

}

$data = file_get_contents("php://input");
if(isset($data)){
  $send = json_decode($data);
  $uname = $send->username;
  $passwd = $send->password;
}else {
  echo "NO DATA";
}

$sql = "SELECT * from users where uname = :uname and passwd = :passwd";
try {
  $stmt = $conn->prepare($sql);
  $stmt-> bindParam(':uname', $uname,PDO::PARAM_STR);
  $stmt-> bindParam(':passwd',$passwd,PDO::PARAM_STR);
  $stmt -> execute();
  $cnt = $stmt -> rowCount();
  $data =  $stmt->fetch(PDO::FETCH_ASSOC);
  if($cnt == 1){
    $json = array(
      'uid' => $data['userid'],
      'username' => $data['uname'],
      'email' => $data['email'],
      'phone' => $data['phone']
    );
  }else{
    $json = array(
      'msg'=>'401'
    );
  }

} catch (Exception $e) {
  echo $e->getMessage();
}


echo json_encode($json);



 ?>
